# p11SSA #



Prezentul proiect reprezintă o extindere a conceptului clasic acoperit de standardul PKCS11, prin înlocuirea tokenului (soluție de securitate hardware) cu un server cu capabilități criptografice (soluție de securitate software), acesta permițând aplicațiilor client de tip PKCS11 să consume serviciile sale.

Proiectul nu urmărește replicarea exhaustivă a funcționalităților unui token, ci dorește să demonstreze validitatea abordării mai sus menționate, prin folosirea unor chei existente la nivelul serverului pentru efectuarea anumitor operații de semnare/criptare, expunând ca interfață o bibliotecă dinamică, interoperabilă cu clienți ce suportă PKCS11.

#### Privire de ansamblu asupra componentelor:

![Scheme](images/block.png)

## 1. p11ssa.dll ##

#### Privire de ansamblu asupra componentelor:

![Scheme](images/conn.png)
 

        După cum se poate observa, rolul acestei biblioteci dinamice este să preia apelurile aplicației client(1), să le encodeze(2,3) conform protocolului din secțiunea 1.2, iar mai apoi să le trimită către service prin intermediul componentei DllClient(4,5), răspunsul venit de la acesta(6,7) fiind prelucrat direct în sensul PKCS11 și rezultatele returnate(8).

### 1.1 frontEnd – practic un wrapper PKCS11, la nivelul acestuia sunt suportate următoarele funcții din standardul PKCS11, v2.40:

-  **Management de obiecte:** C\ __FindObjectsInit_, C\__ FindObjects_, _C\_FindObjectsFinal,_ C\__GetAttributeValue_;
-  **Funcții caracter general:** C\ __Initialize_, C\__ Finalize_, C_\_GetInfo,_ C_\_GetFunctionList_;
-  **Management de sloturi:** C\ __GetSlotList_, C\__ GetSlotInfo_, C_\_GetTokenInfo,_ C_\_GetMechanismList,_ C\__GetMechanismInfo_;
-  **Management de sesiuni:** C\ __OpenSession_, C\__ CloseSession_, C\ __CloseAllSessions_, C\__ GetSessionInfo_, C\ __Login_, C\__ Logout_;
-  **Operații criptografice:** C\ __SignInit_, C\__ Sign_, C\ __SignUpdate_, C\__ SignFinal_, C\ __DigestInit_, C\__ Digest_, C\ __DigestUpdate_, C\__ DigestFinal_, C\ __DecryptInit_, C\__ Decrypt_, C\ __DecryptUpdate_, C\__ DecryptFinal_, C\ __EncryptInit_, C\__ Encrypt_, C\ __EncryptUpdate_, C\__ EncryptFinal_.
-  diferență (deși transparentă pentru aplicația client) în sensul PKCS11 pentru gestiunea obiectelor o reprezintă funcțiile C\_Find – în loc ca frontEnd să fie un simplu wrapper, care să apeleze serverul pentru iterarea clientului prin handlere, acesta reține într-o structură internă, în urma apelării C\_ _FindObjectsInit_, o listă de handlere, pe care mai apoi o gestionează local în cazul apelurilor C\__FindObjects_și_C\_FindObjectsFinal_

### 1.2 denCoder –  componenta care realizează operația de codificare;

- deoarece nu toate argumentele funcțiilor sunt necesare serverului (de exemplu, valoarea pointerilor către locațiile de memorie alocate de către aplicația client p11), acestea nu sunt incluse în rezultatul codificării;
- totuși, pentru posibilitatea efectuării schimbărilor de protocol independent de codul scris în frontEnd, este expusă funcția _encode_, care are rol de dispatcher și acceptă un număr variabil de argumente; prin urmare, toate funcțiile p11 vor apela această funcție cu toate argumentele lor, gestiunea lor efectuându-se integral și transparent, separat de frontEnd;
- componentă de tip singleton.

#### Schema de codificare:

![Scheme](images/packet.png)

#### Definiții:

Offset: zonă de memorie alocată la începutul bufferului, a cărei dimensiune este aleasă de utilizator (pentru exemplificare presupunem N numărul de **bytes** alocați de utilizator pentru această zonă);

FID (Function ID): Reprezintă id-ul funcției care se encodează în bufferul curent. Dimensiunea este       constantă, de 1 **byte** (exemplificat în figura cu P), prin urmare se pot encoda funcții ale căror id-uri sunt numere naturale din intervalul [0 - 255]. Id-urile tuturor funcțiilor implementate se găsesc în headerul &quot;Encoder.h&quot;;

Payload: Reprezintă encodarea argumentelor funcției atașate bufferului curent. Lungimea este variabilă (exemplificată în figura cu Q) în funcție de numărul de argumente și lungimea acestora. Ordinea în care argumentele se encodează este cea descrisă în PKCS #11 Cryptographic Token Interface Base Specification Version 2.40 cu mențiunea că în cazul în care trebuie encodat un șir _variabil_ de octeți _lungimea șirului precedă_ șirul efectiv de octeți.

Exemplu:

Să considerăm funcția C\_Encrypt. Semnătura funcției este:

unsigned char\* encodeC\_Encrypt(CK\_SESSION\_HANDLE hSession,CK\_ULONG ulDataLen,CK\_BYTE\_PTR pData,CK\_ULONG\_PTR pulEncryptedDataLen,CK\_BYTE\_PTR pEncryptedData,unsigned char encodedLength);

CK\_SESSION\_HANDLE hSession este handle-ul la sesiunea curentă. Are tipul unsigned int, deci ocupă 4 bytes.

CK\_ULONG ulDataLen este lungimea șirului de bytes care se va cripta. În standard, acest argument se află după pointerul la date, însă în bufferul de encodare ordinea a fost schimbată, astfel el va preceda șirul.

CK\_BYTE\_PTR pData este pointerul la datele care vor fi criptate. Nu se va encoda pointerul efectiv, dat fiind faptul că utilizatorul și serverul lucrează în mod evident pe zone diferite de memorie însă se va encoda șirul de octeți aflat pe mașina utilizatorului la adresa indicată de pointer.

CK\_ULONG\_PTR pulEncryptedDataLen reprezintă un pointer la lungimea șirului de octeți a șirului criptat de bytes. Analog cazului precedent, nu se transmite pointerul efectiv, ci valoarea indicată de acesta. Se transmite înaintea șirului efectiv. Ocupă 4 bytes, fiind de tip unsigned int.

CK\_BYTE\_PTR pEncryptedData reprezintă șirul criptat.

În realitate însă nu se transmite șirul criptat, acesta fiind chiar task-ul serverului, însă se vor transmite lungimea șirului criptat și un șir de octeți de această lungime din motive **descrise în implementarea protocolului**.

unsigned char encodedLength este un argument care nu are legătură cu protocolul propriu, însă se folosește ca indicator pentru mărimea zonei de memorie ce trebuie alocată pentru tot bufferul. Lungimea totală a bufferului se calculează înainte de apelul funcției, după însumarea lungimilor tuturor argumentelor.



### 1.3 DllClient  - realizează comunicația cu serviciul local

- componentă de tip singleton;
- la inițializare (în cadrul C\__Initialize_), se conectează pe localhost:1111 la service, menținând apoi canalul deschis;
- fiecare apel al metodei _gResult_ este **blocant** ;
- pentru trimiterea a N octeți, întâi se trimit 4 octeți care conțin valoarea N, apoi efectiv cei N octeți; se așteaptă primirea a 4 octeți, reprezentând dimensiunea răspunsului, se alocă respectiva dimensiune și apoi se preia răspunsul;
- înainte de trimiterea unei cereri, primii 4 octeți ai bufferului codificat de denCoder sunt completați cu Pid-ul procesului; acest comportament va fi explicat in secțiunea 2.2;
- la C\_Finalize, închide canalul.



## 2. p11SSA-client ##

Privire de ansamblu asupra componentelor

![Scheme](images/client.png)

### 2.1 Service – threadul principal al serviciului

- componenta ascultă pe localhost:1111, stabilind câte o conexiune cu fiecare aplicație în parte (în urma apelului acestora la C\__Initialize_);
- imediat după stabilirea conexiunii, lansează în execuție un nou thread, componenta ServiceThreadInstance; toate cererile ulterioare ale aplicației vor fi onorate de acest thread, Service punându-se iar în starea de ascultare;
- în momentul în care o componentă ServiceThreadInstance iese din execuție, execută cleanup.

### 2.2 ServiceThreadInstance – componentă tip thread care se ocupă de fiecare aplicație în parte

- intră în execuție imediat după stabilirea primului contact cu p11ssa.dll și onorează toate cererile acesteia, până la apelarea C\_Finalize de către client;
- prin intermediul componentelor DllServer și TLSClient, comunică cu p11ssa.dll, respectiv SSA( **blocant** );
- ciclul său presupune așteptarea unei comenzi de la client, efectuarea verificărilor folosind SessionManager, trimiterea și așteptarea comenzilor către SSA, interpretarea rezultatelor pentru actualizarea componentei SessionManager și trimiterea răspunsului înapoi la client;
- efectuează o translație, în sensul în care cere 20 de octeți de la SSA la apelarea C\_Initialize; acest ChannelID va înlocui cei 4 octeți de Pid pentru cererile ulterioare.

![Scheme](images/pack_leg.png)

### 2.3 SessionManager – componentă care asigură securitatea cererilor la nivel local

- ține o listă cu Pid-ul fiecărui proces care are o conexiune activă cu serviciul; pentru fiecare intrare, există o listă cu sesiunile deschise și un ChannelID (20 octeți);
- atunci când se primește o cerere și aceasta conține id-ul unei sesiuni (de exemplu C\__GetSessionInfo_), se verifică faptul că acel id de sesiune chiar aparține procesului respectiv;
- după ce se primește răspunsul de la SSA, în funcție de tipul funcției, își modifică tabelele, efectuând un fel de &quot;snooping&quot; pe răspuns (de exemplu, după o cerere reușită C\__OpenSession_, introduce sesiunea respectivă în lista procesului);
- componentă tip singleton.