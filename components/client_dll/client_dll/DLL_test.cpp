#include "pkcs11.h"
#include <stdio.h>
#include <string.h>
using namespace std;




int main()
{
	CK_FUNCTION_LIST_PTR pFunctionList;
	CK_RV rv;


	rv = C_GetFunctionList(&pFunctionList);
	if (rv != CKR_OK)
		return rv;

	rv = pFunctionList->C_Initialize(nullptr);
	if (rv != CKR_OK)
		return rv;


	CK_SLOT_ID_PTR pSlotList = nullptr;
	CK_ULONG cati;
	rv = pFunctionList->C_GetSlotList(true, pSlotList, &cati);
	if (rv != CKR_OK)
		return rv;
	if (cati == 0)
		return -2;

	pSlotList = new CK_SLOT_ID[cati * sizeof(CK_SLOT_ID)];
	rv = pFunctionList->C_GetSlotList(true, pSlotList, &cati);
	if (rv != CKR_OK)
		return rv;

	

	CK_SLOT_INFO infoSlot;
	rv = pFunctionList->C_GetSlotInfo(pSlotList[0], &infoSlot);
	if (rv != CKR_OK)
		return -1;

	CK_TOKEN_INFO infoToken;
	rv = pFunctionList->C_GetTokenInfo(pSlotList[0], &infoToken);
	if (rv != CKR_OK)
		return rv;

	CK_SESSION_HANDLE sesiune;
	rv = pFunctionList->C_OpenSession(pSlotList[0], CKF_RW_SESSION | CKF_SERIAL_SESSION, nullptr, nullptr, &sesiune);
	if (rv != CKR_OK)
		return rv;


	CK_SESSION_INFO infoSesiune;
	rv = pFunctionList->C_GetSessionInfo(sesiune, &infoSesiune);
	if (rv != CKR_OK)
		return rv;


	CK_OBJECT_HANDLE_PTR hObject = new CK_OBJECT_HANDLE[1];
	CK_ULONG ulObjectCount = 0;

	//test cheie privata nelogat
	CK_ULONG priv = CKO_PRIVATE_KEY;
	CK_ATTRIBUTE cheia_privata[] = {

		CKA_CLASS, &priv, sizeof(priv)

	};
	/*rv = pFunctionList->C_FindObjectsInit(sesiune, cheia_privata, 1);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjects(sesiune, hObject, 1, &ulObjectCount);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjectsFinal(sesiune);
	if (rv != CKR_OK)
		return -1;


	CK_ULONG x509 = CKC_X_509;
	CK_ULONG cert_type = CKO_CERTIFICATE;
	CK_ATTRIBUTE certificat[] = {
		CKA_CLASS, &cert_type, sizeof(cert_type),
		CKA_CERTIFICATE_TYPE, &x509, sizeof(x509)

	};
	rv = pFunctionList->C_FindObjectsInit(sesiune, certificat, 1);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjects(sesiune, hObject, 1, &ulObjectCount);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjectsFinal(sesiune);
	if (rv != CKR_OK)
		return -1;
		*/



	CK_UTF8CHAR_PTR pin = new CK_UTF8CHAR[5];
	memcpy(pin, "test", 4);
	rv = pFunctionList->C_Login(sesiune, CKU_USER, pin, 4);
	delete[] pin;
	if (rv != CKR_OK)
		return -1;


	//test cheie privata logat
	rv = pFunctionList->C_FindObjectsInit(sesiune, cheia_privata, 1);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjects(sesiune, hObject, 1, &ulObjectCount);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjectsFinal(sesiune);
	if (rv != CKR_OK)
		return -1;
	CK_OBJECT_HANDLE privata = hObject[0];


	CK_ATTRIBUTE t_id[] = {

		CKA_ID, nullptr, 0

	};
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], t_id, 1);
	if (rv != CKR_OK)
		return -1;
	t_id->pValue = new CK_BYTE[t_id->ulValueLen];
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], t_id, 1);
	if (rv != CKR_OK)
		return -1;

	CK_ATTRIBUTE t_subject[] = {

		CKA_SUBJECT, nullptr, 0

	};
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], t_subject, 1);
	if (rv != CKR_OK)
		return -1;
	t_subject->pValue = new CK_BYTE[t_subject->ulValueLen];
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], t_subject, 1);
	if (rv != CKR_OK)
		return -1;


	priv = CKO_CERTIFICATE;
	CK_ATTRIBUTE cert[] = {
		CKA_CLASS, &priv, sizeof(priv),
		t_id[0].type, t_id[0].pValue, t_id[0].ulValueLen
	};
	rv = pFunctionList->C_FindObjectsInit(sesiune, cert, 2);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjects(sesiune, hObject, 1, &ulObjectCount);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjectsFinal(sesiune);
	if (rv != CKR_OK)
		return -1;
	
	priv = CKO_PUBLIC_KEY;
	CK_ATTRIBUTE pubkey[] = {
		CKA_CLASS, &priv, sizeof(priv),
		t_id[0].type, t_id[0].pValue, t_id[0].ulValueLen
	};
	rv = pFunctionList->C_FindObjectsInit(sesiune, pubkey, 2);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjects(sesiune, hObject, 1, &ulObjectCount);
	if (rv != CKR_OK)
		return -1;
	rv = pFunctionList->C_FindObjectsFinal(sesiune);
	if (rv != CKR_OK)
		return -1;
	

	CK_ATTRIBUTE ck_valoare[] = {
		CKA_VALUE, nullptr, 0
	};
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], ck_valoare, 1);
	if (rv != CKR_OK)
		return -1;
	ck_valoare->pValue = new CK_BYTE[ck_valoare->ulValueLen];
	rv = pFunctionList->C_GetAttributeValue(sesiune, hObject[0], ck_valoare, 1);
	if (rv != CKR_OK)
		return -1;

	CK_ATTRIBUTE ck_keytype[] = {
		CKA_KEY_TYPE, nullptr, 0
	};
	rv = pFunctionList->C_GetAttributeValue(sesiune, privata, ck_keytype, 1);
	if (rv != CKR_OK)
		return -1;
	ck_keytype->pValue = new CK_BYTE[ck_keytype->ulValueLen];
	rv = pFunctionList->C_GetAttributeValue(sesiune, privata, ck_keytype, 1);
	if (rv != CKR_OK)
		return -1;



	CK_MECHANISM mechanism = {

		CKM_RSA_PKCS, NULL_PTR, 0

	};
	CK_OBJECT_HANDLE id = privata;
	rv = pFunctionList->C_SignInit(sesiune, &mechanism, id);
	if (rv != CKR_OK)
		return -1;

	CK_BYTE data[] = { 0x00, 0x01, 0x02,0x03 };
	CK_ULONG lsemnatura = 0;
	rv = pFunctionList->C_Sign(sesiune, data, 4, NULL_PTR, &lsemnatura);
	if (rv != CKR_OK)
		return -1;

	CK_BYTE_PTR rez = new CK_BYTE[lsemnatura];
	rv = pFunctionList->C_Sign(sesiune, data, 4, rez, &lsemnatura);
	delete[] rez;
	if (rv != CKR_OK)
		return -1;
	

	rv = pFunctionList->C_Logout(sesiune);
	if (rv != CKR_OK)
		return -1; 
	rv = pFunctionList->C_CloseSession(sesiune);
	if (rv != CKR_OK)
		return rv;

	rv = pFunctionList->C_CloseAllSessions(pSlotList[0]);
	if (rv != CKR_OK)
		return rv;

	rv = pFunctionList->C_Finalize(nullptr);
	if (rv != CKR_OK)
		return rv;

	return 0;
}
