
#include <stdlib.h>

#include "tcpclient.h"
#include "TCPStream.h"
#include "DLLClient.h"
#include "../include/pkcs11.h"
DLLClient * DLLClient::gInstance()
{
	if (client == nullptr)
		client = new DLLClient();
	return client;
}

void DLLClient::dInstance()
{
	if (client != nullptr)
		delete client;
	client = nullptr;
}

DLLClient* DLLClient::client = nullptr;





unsigned char * DLLClient::gResult(unsigned char * comanda, int lComanda, int* lResult)
{

	//ei au deja in lcomanda lungimea prealocata de la plesa!!
	//adica plesa aloca in avans 4 octeti, inainte de payload
	//plesa pune in lcomanda (lungimea payload + 4) (adica pt pid!!!), nu considera si cei 4 octeti de lungime
	unsigned char * Result = nullptr;
	if (!canal)
	{
		*lResult = 4;
		Result = new unsigned char[*lResult];
		unsigned long eroare = CKR_GENERAL_ERROR;
		memcpy(Result, &eroare, 4);
		return Result;
	}

	memcpy(comanda, &lComanda, 4);
	if (!canal->send(comanda, 4))
	{
		*lResult = 4;
		Result = new unsigned char[*lResult];
		unsigned long eroare = CKR_GENERAL_ERROR;
		memcpy(Result, &eroare, 4);
		return Result;
	}

	DWORD pid = GetCurrentProcessId();
	memcpy(comanda, &pid, 4);
	if (!canal->send(comanda, lComanda))
	{
		*lResult = 4;
		Result = new unsigned char[*lResult];
		unsigned long eroare = CKR_GENERAL_ERROR;
		memcpy(Result, &eroare, 4);
		return Result;
	}

	unsigned char gSize[4];
	if (!canal->receive(gSize, 4))
	{
		*lResult = 4;
		Result = new unsigned char[*lResult];
		unsigned long eroare = CKR_GENERAL_ERROR;
		memcpy(Result, &eroare, 4);
		return Result;
	}
	memcpy(lResult, &gSize, 4);


	Result = new unsigned char[*lResult];
	if (!canal->receive(Result, *lResult))
	{
		*lResult = 4;
		Result = new unsigned char[*lResult];
		unsigned long eroare = CKR_GENERAL_ERROR;
		memcpy(Result, &eroare, 4);
		return Result;
	}

	return Result;

}

DLLClient::DLLClient()
{
	connector = new TCPClient();
	canal = connector->connect((unsigned short)1111, "127.0.0.1");//port cunoscut
}

DLLClient::~DLLClient()
{
	delete canal;
	delete connector;
}