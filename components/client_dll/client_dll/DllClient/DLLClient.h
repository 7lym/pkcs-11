#pragma once
class DLLClient
{
public:
	static DLLClient * gInstance();
	static void dInstance();
	unsigned char * gResult(unsigned char * comanda, int lComanda, int* lResult);
private:
	DLLClient();
	~DLLClient();
	static DLLClient * client;
	class TCPStream * canal = nullptr;
	class TCPClient * connector = nullptr;
};