#include <string.h>
#include <WinSock2.h>
#include <ws2tcpip.h>
#include "TCPClient.h"
#include "TCPStream.h"
#pragma comment(lib,  "ws2_32.lib") 
class TCPStream* TCPClient::connect(unsigned short port, const char* server)
{

	struct sockaddr_in address;

	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	if (resolveHostName(server, &(address.sin_addr)) != 0) {
		inet_pton(PF_INET, server, &(address.sin_addr));
	}
	SOCKET sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (::connect(sd, (struct sockaddr*)&address, sizeof(address)) != 0) {
		return NULL;
	}
	return new TCPStream(sd, &address);
}

TCPClient::TCPClient()
{
	WSADATA wsaData;

	::WSAStartup(MAKEWORD(2, 2), (LPWSADATA)(&wsaData));
}

TCPClient::~TCPClient()
{
	::WSACleanup();
}



int TCPClient::resolveHostName(const char* hostname, struct in_addr* addr) 
{
    struct addrinfo *res;
 
    int result = getaddrinfo (hostname, NULL, NULL, &res);
    if (result == 0) {
        memcpy(addr, &((struct sockaddr_in *) res->ai_addr)->sin_addr, 
               sizeof(struct in_addr));
        freeaddrinfo(res);
    }
    return result;
}