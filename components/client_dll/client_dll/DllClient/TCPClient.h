

class TCPClient
{
  public:
    class TCPStream* connect(unsigned short port, const char* server);
	TCPClient();
	~TCPClient();
  private:
    int resolveHostName(const char* host, struct in_addr* addr);
};