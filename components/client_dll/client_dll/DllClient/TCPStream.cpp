
#include <sys/types.h>
#include "TCPStream.h"
#include <ws2tcpip.h>

TCPStream::TCPStream(SOCKET sd, struct sockaddr_in* address) : m_sd(sd) {
    char ip[50];
    inet_ntop(PF_INET, (struct in_addr*)&(address->sin_addr.s_addr), ip, sizeof(ip)-1);
    m_peerIP = ip;
    m_peerPort = ntohs(address->sin_port);
}

TCPStream::~TCPStream()
{
    closesocket(m_sd);
}

bool TCPStream::send(unsigned char* buffer, int len)
{
	int cat = 0;
	while (len > 0)
	{
		cat = ::send(m_sd, (const char *)(buffer + cat), len, 0);
		if (cat < 0)
			return false;
		len -= cat;
	}
	return true;
}

bool TCPStream::receive(unsigned char* buffer, int len)
{
	int cat = 0;
	while (len > 0)
	{
		cat = ::recv(m_sd, (char *)(buffer + cat), len, 0);
		if (cat < 0)
			return false;
		len -= cat;
	}
	return true;
}
