
#include <string>
#include <WinSock2.h>
 
class TCPStream
{
    SOCKET     m_sd;
    std::string  m_peerIP;
    unsigned short     m_peerPort;
 
  public:

	TCPStream(SOCKET sd, struct sockaddr_in* address);
    ~TCPStream();
 
	bool send(unsigned char* buffer, int len);
	bool receive(unsigned char* buffer, int len);
 
	std::string getPeerIP() { return m_peerIP; }
	unsigned short getPeerPort() { return m_peerPort; }
 
private:
	TCPStream(const TCPStream& stream);
};