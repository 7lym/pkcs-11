#include "Encoder.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>




Encoder::Encoder(unsigned long offset)
{
	mod = 32 / 8;
	this->offset = offset;
}
Encoder* Encoder::gInstance(unsigned int offset)
{
	if (Encoder::hins == nullptr)
		hins = new Encoder(offset);
	return hins;
}
Encoder * Encoder::hins = nullptr;

void Encoder::dInstance()
{
	if (hins != nullptr)
		delete hins;
	hins = nullptr;
}
unsigned char* Encoder::encodeC_Initialize(CK_VOID_PTR pInitArgs, unsigned long encodedLength)
{
	unsigned long last = this->offset;
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	encodedString[last] = En_C_Initialize;
	last = last + 1;
	for (unsigned int i = last; i < last + mod; i++)
	{
		encodedString[i] = 0;
	}
	last = last + 2* mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Initialize\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Finalize(CK_VOID_PTR pReserved, unsigned long encodedLength)
{
	unsigned long last = this->offset;
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	encodedString[last] = En_C_Finalize;
	last = last + 1;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Finalize\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetFunctionList(CK_FUNCTION_LIST_PTR_PTR ppFunctionList, unsigned long encodedLength)
{
	unsigned long last = this->offset;
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	encodedString[last] = En_C_GetFunctionList;
	last = last + 1;
	for (unsigned int i = last; i < last + mod; i++)
	{
		encodedString[i] = 0;
	}
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetFunctionList\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetInfo(CK_INFO_PTR pInfo, unsigned long encodedLength)
{
	unsigned long last = this->offset;
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	encodedString[last] = En_C_GetInfo;
	last = last + 1;
	memcpy(encodedString + last, &pInfo->cryptokiVersion, 2);
	last = last + 2;
	memcpy(encodedString + last, &pInfo->manufacturerID, 32);
	last = last + 32;
	memcpy(encodedString + last, &pInfo->flags, mod);
	last = last + mod;
	memcpy(encodedString + last, &pInfo->libraryDescription, 32);
	last = last + 32;
	memcpy(encodedString + last, &pInfo->libraryVersion, 2);
	last = last + 2;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetInfo\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_GetSlotList(CK_BBOOL tokenPresent, CK_ULONG_PTR pulCount, CK_SLOT_ID_PTR pSlotList, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetSlotList;
	last = last + 1;
	memcpy(encodedString + last, &tokenPresent, 1);
	last = last + 1;
	memcpy(encodedString + last, pulCount, mod);
	last = last + mod;
	memcpy(encodedString + last, pSlotList, mod * (*pulCount));
	last = last + mod * (*pulCount);
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetSlotList\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetSlotInfo(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetSlotInfo;
	last = last + 1;
	memcpy(encodedString + last, &slotID, mod);
	last = last + mod;
	memcpy(encodedString + last, &pInfo->slotDescription, 64);
	last = last + 64;
	memcpy(encodedString + last, &pInfo->manufacturerID, 32);
	last = last + 32;
	memcpy(encodedString + last, &pInfo->flags, mod);
	last = last + mod;
	memcpy(encodedString + last, &pInfo->hardwareVersion, 2);
	last = last + 2;
	memcpy(encodedString + last, &pInfo->firmwareVersion, 2);
	last = last + 2;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetSlotInfo\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetMechanismList(CK_SLOT_ID slotID, CK_ULONG_PTR pulCount, CK_MECHANISM_TYPE_PTR pMechanismList, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetMechanismList;
	last = last + 1;
	memcpy(encodedString + last, pMechanismList, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetMechanismList\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetMechanismInfo(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetMechanismInfo;
	last = last + 1;
	memcpy(encodedString + last, &type, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetMechanismInfo\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetTokenInfo(CK_SLOT_ID slotID, CK_TOKEN_INFO_PTR pInfo, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetTokenInfo;
	last = last + 1;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetTokenInfo\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_OpenSession(CK_SLOT_ID slotID, CK_FLAGS flags, CK_VOID_PTR pApplication, CK_NOTIFY Notify, CK_SESSION_HANDLE_PTR phSession, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_OpenSession;
	last = last + 1;
	memcpy(encodedString + last, &flags, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_OpenSession\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_CloseSession(CK_SESSION_HANDLE hSession, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_CloseSession;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_CloseSession\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_CloseAllSessions(CK_SLOT_ID slotID, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_CloseAllSessions;
	last = last + 1;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_CloseAllSessions\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetSessionInfo(CK_SESSION_HANDLE hSession, CK_SESSION_INFO_PTR pInfo, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetSessionInfo;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetSessionInfo\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Login(CK_SESSION_HANDLE hSession, CK_USER_TYPE userType, CK_ULONG ulPinLen, CK_UTF8CHAR_PTR pPin, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Login;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &userType, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulPinLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pPin, ulPinLen);
	last = last + ulPinLen;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Login\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Logout(CK_SESSION_HANDLE hSession, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Logout;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Logout\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_FindObjectsInit(CK_SESSION_HANDLE hSession, CK_ULONG ulCount, CK_ATTRIBUTE_PTR pTemplate, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_FindObjectsInit;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulCount, mod);
	last = last + mod;
	for (unsigned long i = 0; i < ulCount; i++)
	{
		memcpy(encodedString + last, &pTemplate[i].type, mod);
		last = last + mod;
		memcpy(encodedString + last, &pTemplate[i].ulValueLen, mod);
		last = last + mod;
		memcpy(encodedString + last, pTemplate[i].pValue, pTemplate[i].ulValueLen);
		last = last + pTemplate[i].ulValueLen;
	}
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_FindObjectsInit\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_FindObjects(CK_SESSION_HANDLE hSession, CK_ULONG ulMaxObjectCount, CK_ULONG_PTR pulObjectCount, CK_OBJECT_HANDLE_PTR phObject, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_FindObjects;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulMaxObjectCount, mod);
	last = last + mod;
	memcpy(encodedString + last, pulObjectCount, mod);
	last = last + mod;
	memcpy(encodedString + last, phObject, *(pulObjectCount) * mod);
	last = last + *(pulObjectCount) * mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_FindObjects\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_FindObjectsFinal(CK_SESSION_HANDLE hSession, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_FindObjectsFinal;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_FindObjectsFinal\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_GetAttributeValue(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ULONG ulCount, CK_ATTRIBUTE_PTR pTemplate, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_GetAttributeValue;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &hObject, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulCount, mod);
	last = last + mod;
	for (unsigned long i = 0; i < ulCount; i++)
	{
		memcpy(encodedString + last, &pTemplate[i].type, mod);
		last = last + mod;
		memcpy(encodedString + last, &pTemplate[i].ulValueLen, mod);
		last = last + mod;
		memcpy(encodedString + last, pTemplate[i].pValue, pTemplate[i].ulValueLen);
		last = last + pTemplate[i].ulValueLen;
	}
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_GetAttributeValue\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_EncryptInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_EncryptInit;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->mechanism, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->ulParameterLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pMechanism->pParameter, pMechanism->ulParameterLen);
	last = last + pMechanism->ulParameterLen;
	memcpy(encodedString + last, &hKey, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_EncryptInit\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_Encrypt(CK_SESSION_HANDLE hSession, CK_ULONG ulDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulEncryptedDataLen, CK_BYTE_PTR pEncryptedData, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Encrypt;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pData, ulDataLen);
	last = last + ulDataLen;
	memcpy(encodedString + last, pulEncryptedDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pEncryptedData, *(pulEncryptedDataLen));
	last = last + (*(pulEncryptedDataLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Encrypt\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_EncryptUpdate(CK_SESSION_HANDLE hSession, CK_ULONG ulPartLen, CK_BYTE_PTR pPart, CK_ULONG_PTR pulEncryptedPartLen, CK_BYTE_PTR pEncryptedPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_EncryptUpdate;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pPart, ulPartLen);
	last = last + ulPartLen;
	memcpy(encodedString + last, pulEncryptedPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pEncryptedPart, *(pulEncryptedPartLen));
	last = last + (*(pulEncryptedPartLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_EncryptUpdate\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_EncryptFinal(CK_SESSION_HANDLE hSession, CK_ULONG_PTR pulLastEncryptedPartLen, CK_BYTE_PTR pLastEncryptedPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_EncryptFinal;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, pulLastEncryptedPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pLastEncryptedPart, *(pulLastEncryptedPartLen));
	last = last + (*(pulLastEncryptedPartLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_EncryptFinal\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_DecryptInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DecryptInit;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->mechanism, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->ulParameterLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pMechanism->pParameter, pMechanism->ulParameterLen);
	last = last + pMechanism->ulParameterLen;
	memcpy(encodedString + last, &hKey, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DecryptInit\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Decrypt(CK_SESSION_HANDLE hSession, CK_ULONG ulEncryptedDataLen, CK_BYTE_PTR pEncryptedData, CK_ULONG_PTR pulDataLen, CK_BYTE_PTR pData, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Decrypt;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulEncryptedDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pEncryptedData, ulEncryptedDataLen);
	last = last + ulEncryptedDataLen;
	memcpy(encodedString + last, pulDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pData, *(pulDataLen));
	last = last + (*(pulDataLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Decrypt\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_DecryptUpdate(CK_SESSION_HANDLE hSession, CK_ULONG ulEncryptedPartLen, CK_BYTE_PTR pEncryptedPart, CK_ULONG_PTR pulPartLen, CK_BYTE_PTR pPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DecryptUpdate;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulEncryptedPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pEncryptedPart, ulEncryptedPartLen);
	last = last + ulEncryptedPartLen;
	memcpy(encodedString + last, pulPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pPart, *(pulPartLen));
	last = last + (*(pulPartLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DecryptUpdate\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_DecryptFinal(CK_SESSION_HANDLE hSession, CK_ULONG_PTR pulLastPartLen, CK_BYTE_PTR pLastPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DecryptFinal;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, pulLastPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pLastPart, *(pulLastPartLen));
	last = last + (*(pulLastPartLen));
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DecryptFinal\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_SignInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_SignInit;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->mechanism, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->ulParameterLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pMechanism->pParameter, pMechanism->ulParameterLen);
	last = last + pMechanism->ulParameterLen;
	memcpy(encodedString + last, &hKey, mod);
	last = last + mod;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_SignInit\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Sign(CK_SESSION_HANDLE hSession, CK_ULONG ulDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulSignatureLen, CK_BYTE_PTR pSignature, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Sign;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pData, ulDataLen);
	last = last + ulDataLen;
	memcpy(encodedString + last, pulSignatureLen, mod);
	last = last + mod;
	/*memcpy(encodedString + last, pSignature, *(pulSignatureLen));
	last = last + (*(pulSignatureLen));*/
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Sign\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_SignUpdate(CK_SESSION_HANDLE hSession, CK_ULONG ulPartLen, CK_BYTE_PTR pPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_SignUpdate;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pPart, ulPartLen);
	last = last + ulPartLen;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_SignUpdaten\n");
		return 0;
	}
	return encodedString;

}
unsigned char* Encoder::encodeC_SignFinal(CK_SESSION_HANDLE hSession, CK_ULONG_PTR pulSignatureLen, CK_BYTE_PTR pSignature, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_SignFinal;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, pulSignatureLen, mod);
	last = last + mod;
	/*memcpy(encodedString + last, pSignature, *(pulSignatureLen));
	last = last + (*(pulSignatureLen));*/
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_SignFinal\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_DigestInit(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DigestInit;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->mechanism, mod);
	last = last + mod;
	memcpy(encodedString + last, &pMechanism->ulParameterLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pMechanism->pParameter, pMechanism->ulParameterLen);
	last = last + pMechanism->ulParameterLen;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DigestInit\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_Digest(CK_SESSION_HANDLE hSession, CK_ULONG ulDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulDigestLen, CK_BYTE_PTR pDigest, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_Digest;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulDataLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pData, ulDataLen);
	last = last + ulDataLen;
	memcpy(encodedString + last, pulDigestLen, mod);
	last = last + mod;
	/*memcpy(encodedString + last, pDigest, *(pulDigestLen));
	last = last + (*(pulDigestLen));*/
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_Digest\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_DigestUpdate(CK_SESSION_HANDLE hSession, CK_ULONG ulPartLen, CK_BYTE_PTR pPart, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DigestUpdate;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, &ulPartLen, mod);
	last = last + mod;
	memcpy(encodedString + last, pPart, ulPartLen);
	last = last + ulPartLen;
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DigestUpdate\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encodeC_DigestFinal(CK_SESSION_HANDLE hSession, CK_ULONG_PTR pulDigestLen, CK_BYTE_PTR pDigest, unsigned long encodedLength)
{
	unsigned char* encodedString = (unsigned char*)calloc(encodedLength, sizeof(unsigned char));
	unsigned long last = this->offset;
	encodedString[last] = En_C_DigestFinal;
	last = last + 1;
	memcpy(encodedString + last, &hSession, mod);
	last = last + mod;
	memcpy(encodedString + last, pulDigestLen, mod);
	last = last + mod;
	/*memcpy(encodedString + last, pDigest, *(pulDigestLen));
	last = last + (*(pulDigestLen));*/
	if (last != encodedLength)
	{
		printf("Something is wrong with encodeC_DigestFinal\n");
		return 0;
	}
	return encodedString;
}
unsigned char* Encoder::encode(unsigned long* encodedLength, ...)
{
	*encodedLength = this->offset;
	va_list Args;
	va_start(Args, encodedLength);
	int typeOfFunction = va_arg(Args, int);
	unsigned char* encodedString = nullptr;
	if (typeOfFunction == En_C_Initialize)
	{
		*encodedLength += 1 + 2*(mod);//aici vreau 2 * mod
		CK_VOID_PTR pInitArgs = va_arg(Args, CK_VOID_PTR);
		encodedString = encodeC_Initialize(pInitArgs, *encodedLength);
	}
	if (typeOfFunction == En_C_Finalize)
	{
		CK_VOID_PTR pReserved = va_arg(Args, CK_VOID_PTR);
		*encodedLength += 1;
		encodedString = encodeC_Finalize(pReserved, *encodedLength);
	}
	if (typeOfFunction == En_C_GetFunctionList)
	{
		*encodedLength += 1 + (mod);
		CK_FUNCTION_LIST_PTR_PTR ppFunctionList = va_arg(Args, CK_FUNCTION_LIST_PTR_PTR);
		encodedString = encodeC_GetFunctionList(ppFunctionList, *encodedLength);
	}
	if (typeOfFunction == En_C_GetInfo)
	{
		*encodedLength += 1 + (1 + 1 + 32 + mod + 32 + 1 + 1);
		CK_INFO_PTR pInfo = va_arg(Args, CK_INFO_PTR);
		encodedString = encodeC_GetInfo(pInfo, *encodedLength);
	}
	if (typeOfFunction == En_C_GetSlotList)
	{
		CK_BBOOL tokenPresent = va_arg(Args, bool);
		CK_SLOT_ID_PTR pSlotList = va_arg(Args, CK_SLOT_ID_PTR);
		CK_ULONG_PTR pulCount = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (1 + mod + mod * (*(pulCount)));
		encodedString = encodeC_GetSlotList(tokenPresent, pulCount, pSlotList, *encodedLength);

	}
	if (typeOfFunction == En_C_GetSlotInfo)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		CK_SLOT_INFO_PTR pInfo = va_arg(Args, CK_SLOT_INFO_PTR);
		*encodedLength += 1 + (mod + 64 + 32 + mod + 1 + 1 + 1 + 1);
		encodedString = encodeC_GetSlotInfo(slotID, pInfo, *encodedLength);
	}
	if (typeOfFunction == En_C_GetMechanismList)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		CK_MECHANISM_TYPE_PTR pMechanismList = va_arg(Args, CK_MECHANISM_TYPE_PTR);
		CK_ULONG_PTR pulCount = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + mod;
		encodedString = encodeC_GetMechanismList(slotID, pulCount, pMechanismList, *encodedLength);
	}
	if (typeOfFunction == En_C_GetMechanismInfo)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		CK_MECHANISM_TYPE type = va_arg(Args, CK_MECHANISM_TYPE);
		CK_MECHANISM_INFO_PTR pInfo = va_arg(Args, CK_MECHANISM_INFO_PTR);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_GetMechanismInfo(slotID, type, pInfo, *encodedLength);
	}
	if (typeOfFunction == En_C_GetTokenInfo)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		CK_TOKEN_INFO_PTR pInfo = va_arg(Args, CK_TOKEN_INFO_PTR);
		*encodedLength += 1;
		encodedString = encodeC_GetTokenInfo(slotID, pInfo, *encodedLength);
	}
	if (typeOfFunction == En_C_OpenSession)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		CK_FLAGS flags = va_arg(Args, CK_FLAGS);
		CK_VOID_PTR pApplication = va_arg(Args, CK_VOID_PTR);
		CK_NOTIFY Notify = va_arg(Args, CK_NOTIFY);
		CK_SESSION_HANDLE_PTR phSession = va_arg(Args, CK_SESSION_HANDLE_PTR);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_OpenSession(slotID, flags, pApplication, Notify, phSession, *encodedLength);
	}
	if (typeOfFunction == En_C_CloseSession)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_CloseSession(hSession, *encodedLength);
	}
	if (typeOfFunction == En_C_CloseAllSessions)
	{
		CK_SLOT_ID slotID = va_arg(Args, CK_SLOT_ID);
		*encodedLength += 1;
		encodedString = encodeC_CloseAllSessions(slotID, *encodedLength);
	}
	if (typeOfFunction == En_C_GetSessionInfo)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_SESSION_INFO_PTR pInfo = va_arg(Args, CK_SESSION_INFO_PTR);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_GetSessionInfo(hSession, pInfo, *encodedLength);
	}
	if (typeOfFunction == En_C_Login)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_USER_TYPE userType = va_arg(Args, CK_USER_TYPE);
		CK_UTF8CHAR_PTR pPin = va_arg(Args, CK_UTF8CHAR_PTR);
		CK_ULONG ulPinLen = va_arg(Args, CK_ULONG);
		*encodedLength += 1 + (mod + mod + mod + ulPinLen);
		encodedString = encodeC_Login(hSession, userType, ulPinLen, pPin, *encodedLength);
	}
	if (typeOfFunction == En_C_Logout)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_Logout(hSession, *encodedLength);
	}
	if (typeOfFunction == En_C_FindObjectsInit)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_ATTRIBUTE_PTR pTemplate = va_arg(Args, CK_ATTRIBUTE_PTR);
		CK_ULONG ulCount = va_arg(Args, CK_ULONG);
		unsigned long sum = 0;
		for (unsigned long i = 0; i < ulCount; i++)
		{
			sum = sum + mod + mod + pTemplate[i].ulValueLen;
		}
		*encodedLength += 1 + (mod + mod + sum);
		encodedString = encodeC_FindObjectsInit(hSession, ulCount, pTemplate, *encodedLength);
	}
	if (typeOfFunction == En_C_FindObjects)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_OBJECT_HANDLE_PTR phObject = va_arg(Args, CK_OBJECT_HANDLE_PTR);
		CK_ULONG ulMaxObjectCount = va_arg(Args, CK_ULONG);
		CK_ULONG_PTR pulObjectCount = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + mod + mod * (*(pulObjectCount)));
		encodedString = encodeC_FindObjects(hSession, ulMaxObjectCount, pulObjectCount, phObject, *encodedLength);
	}
	if (typeOfFunction == En_C_FindObjectsFinal)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		*encodedLength += 1 + (mod);
		encodedString = encodeC_FindObjectsFinal(hSession, *encodedLength);
	}
	if (typeOfFunction == En_C_GetAttributeValue)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_OBJECT_HANDLE hObject = va_arg(Args, CK_OBJECT_HANDLE);
		CK_ATTRIBUTE_PTR pTemplate = va_arg(Args, CK_ATTRIBUTE_PTR);
		CK_ULONG ulCount = va_arg(Args, CK_ULONG);
		unsigned long sum = 0;
		for (unsigned long i = 0; i < ulCount; i++)
		{
			sum = sum + mod + mod + pTemplate[i].ulValueLen;
		}
		*encodedLength += 1 + (mod + mod + mod + sum);
		encodedString = encodeC_GetAttributeValue(hSession, hObject, ulCount, pTemplate, *encodedLength);
	}
	if (typeOfFunction == En_C_EncryptInit)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_MECHANISM_PTR pMechanism = va_arg(Args, CK_MECHANISM_PTR);
		CK_OBJECT_HANDLE hKey = va_arg(Args, CK_OBJECT_HANDLE);
		*encodedLength += 1 + (mod + mod + mod + pMechanism->ulParameterLen + mod);
		encodedString = encodeC_EncryptInit(hSession, pMechanism, hKey, *encodedLength);
	}
	if (typeOfFunction == En_C_Encrypt)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulDataLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pEncryptedData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulEncryptedDataLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulDataLen + mod + (*pulEncryptedDataLen));
		encodedString = encodeC_Encrypt(hSession, ulDataLen, pData, pulEncryptedDataLen, pEncryptedData, *encodedLength);

	}
	if (typeOfFunction == En_C_EncryptUpdate)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulPartLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pEncryptedPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulEncryptedPartLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulPartLen + mod + (*pulEncryptedPartLen));
		encodedString = encodeC_EncryptUpdate(hSession, ulPartLen, pPart, pulEncryptedPartLen, pEncryptedPart, *encodedLength);
	}
	if (typeOfFunction == En_C_EncryptFinal)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pLastEncryptedPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulLastEncryptedPartLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + (*pulLastEncryptedPartLen));
		encodedString = encodeC_EncryptFinal(hSession, pulLastEncryptedPartLen, pLastEncryptedPart, *encodedLength);
	}
	if (typeOfFunction == En_C_DecryptInit)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_MECHANISM_PTR pMechanism = va_arg(Args, CK_MECHANISM_PTR);
		CK_OBJECT_HANDLE hKey = va_arg(Args, CK_OBJECT_HANDLE);
		*encodedLength += 1 + (mod + mod + mod + pMechanism->ulParameterLen + mod);
		encodedString = encodeC_DecryptInit(hSession, pMechanism, hKey, *encodedLength);
	}
	if (typeOfFunction == En_C_Decrypt)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pEncryptedData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulEncryptedDataLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulDataLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulEncryptedDataLen + mod + (*pulDataLen));
		encodedString = encodeC_Decrypt(hSession, ulEncryptedDataLen, pEncryptedData, pulDataLen, pData, *encodedLength);
	}
	if (typeOfFunction == En_C_DecryptUpdate)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pEncryptedPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulEncryptedPartLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulPartLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulEncryptedPartLen + mod + (*pulPartLen));
		encodedString = encodeC_DecryptUpdate(hSession, ulEncryptedPartLen, pEncryptedPart, pulPartLen, pPart, *encodedLength);
	}
	if (typeOfFunction == En_C_DecryptFinal)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pLastPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulLastPartLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + (*pulLastPartLen));
		encodedString = encodeC_DecryptFinal(hSession, pulLastPartLen, pLastPart, *encodedLength);
	}
	if (typeOfFunction == En_C_SignInit)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_MECHANISM_PTR pMechanism = va_arg(Args, CK_MECHANISM_PTR);
		CK_OBJECT_HANDLE hKey = va_arg(Args, CK_OBJECT_HANDLE);
		*encodedLength += 1 + (mod + mod + mod + pMechanism->ulParameterLen + mod);
		encodedString = encodeC_SignInit(hSession, pMechanism, hKey, *encodedLength);
	}
	if (typeOfFunction == En_C_Sign)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulDataLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pSignature = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulSignatureLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulDataLen + mod /*+ (*pulSignatureLen)*/);
		encodedString = encodeC_Sign(hSession, ulDataLen, pData, pulSignatureLen, pSignature, *encodedLength);
	}
	if (typeOfFunction == En_C_SignUpdate)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulPartLen = va_arg(Args, CK_ULONG);
		*encodedLength += 1 + (mod + mod + ulPartLen);
		encodedString = encodeC_SignUpdate(hSession, ulPartLen, pPart, *encodedLength);
	}
	if (typeOfFunction == En_C_SignFinal)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pSignature = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulSignatureLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod/* + (*pulSignatureLen)*/);
		encodedString = encodeC_SignFinal(hSession, pulSignatureLen, pSignature, *encodedLength);
	}
	if (typeOfFunction == En_C_DigestInit)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_MECHANISM_PTR pMechanism = va_arg(Args, CK_MECHANISM_PTR);
		*encodedLength += 1 + (mod + mod + mod + pMechanism->ulParameterLen);
		encodedString = encodeC_DigestInit(hSession, pMechanism, *encodedLength);
	}
	if (typeOfFunction == En_C_Digest)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pData = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulDataLen = va_arg(Args, CK_ULONG);
		CK_BYTE_PTR pDigest = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulDigestLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod + ulDataLen + mod/* + (*pulDigestLen)*/);
		encodedString = encodeC_Digest(hSession, ulDataLen, pData, pulDigestLen, pDigest, *encodedLength);
	}
	if (typeOfFunction == En_C_DigestUpdate)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pPart = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG ulPartLen = va_arg(Args, CK_ULONG);
		*encodedLength += 1 + (mod + mod + ulPartLen);
		encodedString = encodeC_DigestUpdate(hSession, ulPartLen, pPart, *encodedLength);
	}
	if (typeOfFunction == En_C_DigestFinal)
	{
		CK_SESSION_HANDLE hSession = va_arg(Args, CK_SESSION_HANDLE);
		CK_BYTE_PTR pDigest = va_arg(Args, CK_BYTE_PTR);
		CK_ULONG_PTR pulDigestLen = va_arg(Args, CK_ULONG_PTR);
		*encodedLength += 1 + (mod + mod /*+ (*pulDigestLen)*/);
		encodedString = encodeC_DigestFinal(hSession, pulDigestLen, pDigest, *encodedLength);
	}
	va_end(Args);
	return encodedString;
}
