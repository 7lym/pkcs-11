#ifndef ENCODER_H
//#define _CRT_NO_VA_START_VALIDATION
#include "pkcs11.h"
#include <stdarg.h>
#define En_C_Initialize 1
#define En_C_Finalize 2
#define En_C_GetFunctionList 3
#define En_C_GetInfo 4
#define En_C_GetSlotList 5
#define En_C_GetSlotInfo 6
#define En_C_GetMechanismList 7
#define En_C_GetMechanismInfo 8
#define En_C_GetTokenInfo 9
#define En_C_OpenSession 10
#define En_C_CloseSession 11
#define En_C_CloseAllSessions 12
#define En_C_GetSessionInfo 13
#define En_C_Login 14
#define En_C_Logout 15
//empty 16
#define En_C_FindObjectsInit 17
#define En_C_FindObjects 18
#define En_C_FindObjectsFinal 19
#define En_C_GetAttributeValue 20
#define En_C_EncryptInit 21
#define En_C_Encrypt 22
#define En_C_EncryptUpdate 23
#define En_C_EncryptFinal 24
#define En_C_DecryptInit 25
#define En_C_Decrypt 26
#define En_C_DecryptUpdate 27
#define En_C_DecryptFinal 28
#define En_C_SignInit 29
#define En_C_Sign 30
#define En_C_SignUpdate 31
#define En_C_SignFinal 32
#define En_C_DigestInit 33
#define En_C_Digest 34
#define En_C_DigestUpdate 35
#define En_C_DigestFinal 36
class Encoder
{
private:
	int mod ;
	static Encoder * hins;
    unsigned long offset;
    unsigned char* encodeC_Initialize(CK_VOID_PTR pInitArgs,unsigned long encodedLength);
    unsigned char* encodeC_Finalize(CK_VOID_PTR pReserved,unsigned long encodedLength);
    unsigned char* encodeC_GetFunctionList(CK_FUNCTION_LIST_PTR_PTR ppFunctionList,unsigned long encodedLength);
    unsigned char* encodeC_GetInfo(CK_INFO_PTR pInfo,unsigned long encodedLength);
    unsigned char* encodeC_GetSlotList(CK_BBOOL tokenPresent, CK_ULONG_PTR pulCount, CK_SLOT_ID_PTR pSlotList, unsigned long encodedLength);
    unsigned char* encodeC_GetSlotInfo(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo, unsigned long encodedLength);
    unsigned char* encodeC_GetMechanismList( CK_SLOT_ID slotID, CK_ULONG_PTR pulCount, CK_MECHANISM_TYPE_PTR pMechanismList ,unsigned long encodedLength);
    unsigned char* encodeC_GetMechanismInfo(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo, unsigned long encodedLength);
    unsigned char* encodeC_GetTokenInfo(CK_SLOT_ID slotID,CK_TOKEN_INFO_PTR pInfo,unsigned long encodedLength);
    unsigned char* encodeC_OpenSession(CK_SLOT_ID slotID,CK_FLAGS flags,CK_VOID_PTR pApplication,CK_NOTIFY Notify,CK_SESSION_HANDLE_PTR phSession,unsigned long encodedLength);
    unsigned char* encodeC_CloseSession(CK_SESSION_HANDLE hSession,unsigned long encodedLength);
    unsigned char* encodeC_CloseAllSessions(CK_SLOT_ID slotID,unsigned long encodedLength);
    unsigned char* encodeC_GetSessionInfo(CK_SESSION_HANDLE hSession,CK_SESSION_INFO_PTR pInfo,unsigned long encodedLength);
    unsigned char* encodeC_Login(CK_SESSION_HANDLE hSession,CK_USER_TYPE userType,CK_ULONG ulPinLen,CK_UTF8CHAR_PTR pPin,unsigned long encodedLength);
    unsigned char* encodeC_Logout(CK_SESSION_HANDLE hSession,unsigned long encodedLength);
    unsigned char* encodeC_FindObjectsInit(CK_SESSION_HANDLE hSession,CK_ULONG ulCount,CK_ATTRIBUTE_PTR pTemplate,unsigned long encodedLength);
    unsigned char* encodeC_FindObjects(CK_SESSION_HANDLE hSession,CK_ULONG ulMaxObjectCount,CK_ULONG_PTR pulObjectCount,CK_OBJECT_HANDLE_PTR phObject,unsigned long encodedLength);
    unsigned char* encodeC_FindObjectsFinal(CK_SESSION_HANDLE hSession,unsigned long encodedLength);
    unsigned char* encodeC_GetAttributeValue(CK_SESSION_HANDLE hSession,CK_OBJECT_HANDLE hObject,CK_ULONG ulCount,CK_ATTRIBUTE_PTR pTemplate,unsigned long encodedLength);
    unsigned char* encodeC_EncryptInit(CK_SESSION_HANDLE hSession,CK_MECHANISM_PTR pMechanism,CK_OBJECT_HANDLE hKey,unsigned long encodedLength);
    unsigned char* encodeC_Encrypt(CK_SESSION_HANDLE hSession,CK_ULONG ulDataLen,CK_BYTE_PTR pData,CK_ULONG_PTR pulEncryptedDataLen,CK_BYTE_PTR pEncryptedData,unsigned long encodedLength);
    unsigned char* encodeC_EncryptUpdate(CK_SESSION_HANDLE hSession,CK_ULONG ulPartLen,CK_BYTE_PTR pPart,CK_ULONG_PTR pulEncryptedPartLen,CK_BYTE_PTR pEncryptedPart,unsigned long encodedLength);
    unsigned char* encodeC_EncryptFinal(CK_SESSION_HANDLE hSession,CK_ULONG_PTR pulLastEncryptedPartLen,CK_BYTE_PTR pLastEncryptedPart,unsigned long encodedLength);
    unsigned char* encodeC_DecryptInit(CK_SESSION_HANDLE hSession,CK_MECHANISM_PTR pMechanism,CK_OBJECT_HANDLE hKey,unsigned long encodedLength);
    unsigned char* encodeC_Decrypt(CK_SESSION_HANDLE hSession,CK_ULONG ulEncryptedDataLen,CK_BYTE_PTR pEncryptedData,CK_ULONG_PTR pulDataLen,CK_BYTE_PTR pData,unsigned long encodedLength);
    unsigned char* encodeC_DecryptUpdate(CK_SESSION_HANDLE hSession,CK_ULONG ulEncryptedPartLen,CK_BYTE_PTR pEncryptedPart,CK_ULONG_PTR pulPartLen,CK_BYTE_PTR pPart,unsigned long encodedLength);
    unsigned char* encodeC_DecryptFinal(CK_SESSION_HANDLE hSession,CK_ULONG_PTR pulLastPartLen,CK_BYTE_PTR pLastPart,unsigned long encodedLength);
    unsigned char* encodeC_SignInit(CK_SESSION_HANDLE hSession,CK_MECHANISM_PTR pMechanism,CK_OBJECT_HANDLE hKey,unsigned long encodedLength);
    unsigned char* encodeC_Sign(CK_SESSION_HANDLE hSession,CK_ULONG ulDataLen,CK_BYTE_PTR pData,CK_ULONG_PTR pulSignatureLen,CK_BYTE_PTR pSignature,unsigned long encodedLength);
    unsigned char* encodeC_SignUpdate(CK_SESSION_HANDLE hSession,CK_ULONG ulPartLen,CK_BYTE_PTR pPart,unsigned long encodedLength);
    unsigned char* encodeC_SignFinal(CK_SESSION_HANDLE hSession,CK_ULONG_PTR pulSignatureLen,CK_BYTE_PTR pSignature,unsigned long encodedLength);
    unsigned char* encodeC_DigestInit(CK_SESSION_HANDLE hSession,CK_MECHANISM_PTR pMechanism,unsigned long encodedLength);
    unsigned char* encodeC_Digest(CK_SESSION_HANDLE hSession,CK_ULONG ulDataLen,CK_BYTE_PTR pData,CK_ULONG_PTR pulDigestLen,CK_BYTE_PTR pDigest,unsigned long encodedLength);
    unsigned char* encodeC_DigestUpdate(CK_SESSION_HANDLE hSession,CK_ULONG ulPartLen,CK_BYTE_PTR pPart,unsigned long encodedLength);
    unsigned char* encodeC_DigestFinal(CK_SESSION_HANDLE hSession,CK_ULONG_PTR pulDigestLen,CK_BYTE_PTR pDigest,unsigned long encodedLength);
	Encoder(unsigned long offset);
	Encoder(Encoder&);
public:
	static Encoder* gInstance(unsigned int);
	static void dInstance();
    unsigned char* encode(unsigned long* encodedLength, ... );
};
#endif // ENCODER_H
