#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include "../denCoder/Encoder.h"
#include <iostream>
extern bool isInited;

CK_DEFINE_FUNCTION(CK_RV, C_EncryptInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
	/* Check initialisation */
    if (isInited == false)
    {
        return CKR_CRYPTOKI_NOT_INITIALIZED;
    }

	/* Check Mechanism */
	if (pMechanism->mechanism != CKM_RSA_PKCS
		&& pMechanism->mechanism != CKM_RSA_PKCS_OAEP)
	{
		return CKR_MECHANISM_INVALID;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_EncryptInit, hSession, pMechanism, hKey);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

    /* Return the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	delete[] response;

    return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_Encrypt)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pEncryptedData, CK_ULONG_PTR pulEncryptedDataLen)
{
	/* Check initialisation */
    if (isInited == false)
    {
        return CKR_CRYPTOKI_NOT_INITIALIZED;
    }

	/* Check the buffer data and length */
	if (nullptr == pData || ulDataLen < 0U)
	{
		return CKR_ARGUMENTS_BAD;
	}
	
	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_Encrypt, hSession, pData, ulDataLen, pEncryptedData, pulEncryptedDataLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pEncryptedData)
		{
			memcpy(pulEncryptedDataLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pEncryptedData, response + sizeof(CK_RV), *pulEncryptedDataLen);
		}
	}

	delete[] response;
    return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_EncryptUpdate)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen, CK_BYTE_PTR pEncryptedPart, CK_ULONG_PTR pulEncryptedPartLen)
{
	/* Check initialisation */
    if (isInited == false)
    {
        return CKR_CRYPTOKI_NOT_INITIALIZED;
    }

	/* Check the buffer data and length */
	if (nullptr == pPart || ulPartLen < 0U)
	{
		return CKR_ARGUMENTS_BAD;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_EncryptUpdate, hSession, pPart, ulPartLen, pEncryptedPart, pulEncryptedPartLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pEncryptedPart)
		{
			memcpy(pulEncryptedPartLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pEncryptedPart, response + sizeof(CK_RV), *pulEncryptedPartLen);
		}
	}

	delete[] response;
    return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_EncryptFinal)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pLastEncryptedPart, CK_ULONG_PTR pulLastEncryptedPartLen)
{
	/* Check initialisation */
	if (isInited == false)
	{
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_EncryptFinal, hSession, pLastEncryptedPart, pulLastEncryptedPartLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pLastEncryptedPart)
		{
			memcpy(pulLastEncryptedPartLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pLastEncryptedPart, response + sizeof(CK_RV), *pulLastEncryptedPartLen);
		}
	}

	delete[] response;
	return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_DecryptInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
	/* Check initialisation */
	if (isInited == false)
	{
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	}

	/* Check Mechanism */
	if (pMechanism->mechanism != CKM_RSA_PKCS
		&& pMechanism->mechanism != CKM_RSA_PKCS_OAEP)
	{
		return CKR_MECHANISM_INVALID;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_DecryptInit, hSession, pMechanism, hKey);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Return the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	delete[] response;

	return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_DecryptUpdate)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedPart, CK_ULONG ulEncryptedPartLen, CK_BYTE_PTR pPart, CK_ULONG_PTR pulPartLen)
{
	/* Check initialisation */
	if (isInited == false)
	{
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	}

	/* Check the buffer data and length */
	if (nullptr == pEncryptedPart || ulEncryptedPartLen < 0U)
	{
		return CKR_ARGUMENTS_BAD;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_DecryptUpdate, hSession, pEncryptedPart, ulEncryptedPartLen, pPart, pulPartLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pPart)
		{
			memcpy(pulPartLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pPart, response + sizeof(CK_RV), *pulPartLen);
		}
	}

	delete[] response;
	return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_DecryptFinal)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pLastPart, CK_ULONG_PTR pulLastPartLen)
{
	/* Check initialisation */
	if (isInited == false)
	{
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_DecryptFinal, hSession, pLastPart, pulLastPartLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));
	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pLastPart)
		{
			memcpy(pulLastPartLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pLastPart, response + sizeof(CK_RV), *pulLastPartLen);
		}
	}

	delete[] response;
	return decoded;
}

CK_DEFINE_FUNCTION(CK_RV, C_Decrypt)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pEncryptedData, CK_ULONG ulEncryptedDataLen, CK_BYTE_PTR pData, CK_ULONG_PTR pulDataLen)
{
	/* Check initialisation */
	if (isInited == false)
	{
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	}

	/* Check the buffer data and length */
	if (nullptr == pEncryptedData || ulEncryptedDataLen < 0U)
	{
		return CKR_ARGUMENTS_BAD;
	}

	/* Encode function */
	unsigned long encoded = 0U;
	CK_CHAR_PTR command = Encoder::gInstance(4)->encode(&encoded, En_C_Decrypt, hSession, pEncryptedData, ulEncryptedDataLen, pData, pulDataLen);

	/* Get the response from server */
	int ulCount = 0;
	CK_BYTE_PTR response = DLLClient::gInstance()->gResult(command, encoded, &ulCount);
	free(command);

	/* Check the response */
	CK_RV decoded = CKR_OK;
	memcpy(&decoded, response, sizeof(CK_RV));

	/* Check returned data */
	if (CKR_OK == decoded)
	{
		if (nullptr == pData)
		{
			memcpy(pulDataLen, response + sizeof(CK_RV), sizeof(CK_ULONG));
		}
		else
		{
			memcpy(pData, response + sizeof(CK_RV), *pulDataLen);
		}
	}

	delete[] response;
	return decoded;
}