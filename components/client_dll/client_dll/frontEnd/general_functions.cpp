
#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include "../denCoder/Encoder.h"
#include <WinSock2.h>

bool isInited = false;

CK_FUNCTION_LIST function_list = {
	{ 2,40 },
	&C_Initialize,
	&C_Finalize,
	&C_GetInfo,
	&C_GetFunctionList,
	&C_GetSlotList,
	&C_GetSlotInfo,
	&C_GetTokenInfo,
	&C_GetMechanismList,
	&C_GetMechanismInfo,
	&C_InitToken,//NOT_SUPPORTED
	&C_InitPIN,//NOT_SUPPORTED
	&C_SetPIN,//NOT_SUPPORTED
	&C_OpenSession,
	&C_CloseSession,
	&C_CloseAllSessions,
	&C_GetSessionInfo,
	&C_GetOperationState,//NOT_SUPPORTED
	&C_SetOperationState,//NOT_SUPPORTED
	&C_Login,
	&C_Logout,
	&C_CreateObject,//NOT_SUPPORTED
	&C_CopyObject,//NOT_SUPPORTED
	&C_DestroyObject,//NOT_SUPPORTED
	&C_GetObjectSize,//NOT_SUPPORTED
	&C_GetAttributeValue,
	&C_SetAttributeValue,//NOT_SUPPORTED
	&C_FindObjectsInit,
	&C_FindObjects,
	&C_FindObjectsFinal,
	&C_EncryptInit,
	&C_Encrypt,
	&C_EncryptUpdate,
	&C_EncryptFinal,
	&C_DecryptInit,
	&C_Decrypt,
	&C_DecryptUpdate,
	&C_DecryptFinal,
	&C_DigestInit,
	&C_Digest,
	&C_DigestUpdate,
	&C_DigestKey,//NOT_SUPPORTED
	&C_DigestFinal,
	&C_SignInit,
	&C_Sign,
	&C_SignUpdate,
	&C_SignFinal,
	&C_SignRecoverInit,//NOT_SUPPORTED
	&C_SignRecover,//NOT_SUPPORTED
	&C_VerifyInit,//NOT_SUPPORTED
	&C_Verify,//NOT_SUPPORTED
	&C_VerifyUpdate,//NOT_SUPPORTED
	&C_VerifyFinal,//NOT_SUPPORTED
	&C_VerifyRecoverInit,//NOT_SUPPORTED
	&C_VerifyRecover,//NOT_SUPPORTED
	&C_DigestEncryptUpdate,//NOT_SUPPORTED
	&C_DecryptDigestUpdate,//NOT_SUPPORTED
	&C_SignEncryptUpdate,//NOT_SUPPORTED
	&C_DecryptVerifyUpdate,//NOT_SUPPORTED
	&C_GenerateKey,//NOT_SUPPORTED
	&C_GenerateKeyPair,//NOT_SUPPORTED
	&C_WrapKey,//NOT_SUPPORTED
	&C_UnwrapKey,//NOT_SUPPORTED
	&C_DeriveKey,//NOT_SUPPORTED
	&C_SeedRandom,//NOT_SUPPORTED
	&C_GenerateRandom,//NOT_SUPPORTED
	&C_GetFunctionStatus,//NOT_SUPPORTED
	&C_CancelFunction,//NOT_SUPPORTED
	&C_WaitForSlotEvent//NOT_SUPPORTED
};


CK_DEFINE_FUNCTION(CK_RV, C_Initialize)(CK_VOID_PTR pInitArgs)
{
	if (isInited == true)
		return CKR_CRYPTOKI_ALREADY_INITIALIZED;
	if (pInitArgs != NULL_PTR)
		return CKR_CANT_LOCK;
	//DLLClient::gInstance();
	
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Initialize, pInitArgs);
	
	
	
	DWORD pid = GetCurrentProcessId();
	memcpy(comanda + 4 + 1, &pid, 4);//pid pt popescu
	memcpy(comanda + 4 + 4 + 1, &pid, 4);//pid pt popescu
	
	
	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, 4);

	delete[] raspuns;
	if (decodat == CKR_OK)
		isInited = true;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_Finalize)(CK_VOID_PTR pReserved)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (pReserved != NULL_PTR)
		return CKR_ARGUMENTS_BAD;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Finalize, pReserved);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, 4);
	delete[] raspuns;

	if (decodat == CKR_OK)
	{
		Encoder::dInstance();
		DLLClient::dInstance();
		isInited = false;
	}
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetInfo)(CK_INFO_PTR pInfo)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	if (NULL == pInfo)
		return CKR_ARGUMENTS_BAD;

	pInfo->cryptokiVersion.major = 0x02;
	pInfo->cryptokiVersion.minor = 40;
	memset(pInfo->manufacturerID, ' ', 32);
	memcpy(pInfo->manufacturerID, "ATM@Registered", 14);
	pInfo->flags = 0;
	memset(pInfo->libraryDescription, ' ', 32);
	memcpy(pInfo->libraryDescription, "CrypATMki", 9);
	pInfo->libraryVersion.major = 0x02;
	pInfo->libraryVersion.minor = 40;
	return CKR_OK;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetFunctionList)(CK_FUNCTION_LIST_PTR_PTR ppFunctionList)
{
	if (ppFunctionList == nullptr)
		return CKR_ARGUMENTS_BAD;

	*ppFunctionList = &function_list;
	return CKR_OK;
}


CK_DEFINE_FUNCTION(CK_RV, C_InitToken)(CK_SLOT_ID , CK_UTF8CHAR_PTR , CK_ULONG , CK_UTF8CHAR_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_InitPIN)(CK_SESSION_HANDLE , CK_UTF8CHAR_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SetPIN)(CK_SESSION_HANDLE , CK_UTF8CHAR_PTR , CK_ULONG , CK_UTF8CHAR_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GetOperationState)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SetOperationState)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_OBJECT_HANDLE , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_CreateObject)(CK_SESSION_HANDLE , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_CopyObject)(CK_SESSION_HANDLE , CK_OBJECT_HANDLE , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DestroyObject)(CK_SESSION_HANDLE , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GetObjectSize)(CK_SESSION_HANDLE , CK_OBJECT_HANDLE , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SetAttributeValue)(CK_SESSION_HANDLE , CK_OBJECT_HANDLE , CK_ATTRIBUTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DigestKey)(CK_SESSION_HANDLE , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SignRecoverInit)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SignRecover)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_VerifyInit)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_Verify)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_VerifyUpdate)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_VerifyFinal)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_VerifyRecoverInit)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_VerifyRecover)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DigestEncryptUpdate)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DecryptDigestUpdate)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR pPart, CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SignEncryptUpdate)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DecryptVerifyUpdate)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GenerateKey)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GenerateKeyPair)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_ATTRIBUTE_PTR , CK_ULONG , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_WrapKey)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE , CK_OBJECT_HANDLE , CK_BYTE_PTR , CK_ULONG_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_UnwrapKey)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE , CK_BYTE_PTR , CK_ULONG , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_DeriveKey)(CK_SESSION_HANDLE , CK_MECHANISM_PTR , CK_OBJECT_HANDLE , CK_ATTRIBUTE_PTR , CK_ULONG , CK_OBJECT_HANDLE_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_SeedRandom)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GenerateRandom)(CK_SESSION_HANDLE , CK_BYTE_PTR , CK_ULONG )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_GetFunctionStatus)(CK_SESSION_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_CancelFunction)(CK_SESSION_HANDLE )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
CK_DEFINE_FUNCTION(CK_RV, C_WaitForSlotEvent)(CK_FLAGS , CK_SLOT_ID_PTR , CK_VOID_PTR )
{
	return CKR_FUNCTION_NOT_SUPPORTED;
}
