#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include "../denCoder/Encoder.h"
#include <WinSock2.h>

extern bool isInited;
CK_DEFINE_FUNCTION(CK_RV, C_DigestInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if ((pMechanism->mechanism != CKM_SHA224) && (pMechanism->mechanism != CKM_SHA256) &&
		(pMechanism->mechanism != CKM_SHA384) && (pMechanism->mechanism != CKM_SHA512))
		return CKR_MECHANISM_INVALID;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_DigestInit, hSession, pMechanism);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_Digest)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pDigest, CK_ULONG_PTR pulDigestLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	
	if (pData == nullptr || ulDataLen < 0)
		return CKR_ARGUMENTS_BAD;


	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Digest, hSession, pData, ulDataLen, pDigest, pulDigestLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));

	if (decodat == CKR_OK)
		if (pDigest == nullptr)
			memcpy(pulDigestLen, raspuns + sizeof(CK_RV), sizeof(CK_ULONG));
		else
			memcpy(pDigest, raspuns + sizeof(CK_RV), *pulDigestLen);

	delete[] raspuns;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_DigestUpdate)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (pPart == nullptr || ulPartLen < 1)
		return CKR_ARGUMENTS_BAD;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_DigestUpdate, hSession, pPart, ulPartLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	return decodat;
}





CK_DEFINE_FUNCTION(CK_RV, C_DigestFinal)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pDigest, CK_ULONG_PTR pulDigestLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_DigestFinal, hSession, pDigest, pulDigestLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));

	if (decodat == CKR_OK)
		if (pDigest == nullptr)
			memcpy(pulDigestLen, raspuns + sizeof(CK_RV), sizeof(CK_ULONG));
		else
			memcpy(pDigest, raspuns + sizeof(CK_RV), *pulDigestLen);

	delete[] raspuns;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_SignInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR pMechanism, CK_OBJECT_HANDLE hKey)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	if ((pMechanism->mechanism != CKM_SHA256_RSA_PKCS_PSS) && (pMechanism->mechanism != CKM_SHA384_RSA_PKCS_PSS) &&
		(pMechanism->mechanism != CKM_SHA512_RSA_PKCS_PSS) && (pMechanism->mechanism != CKM_RSA_PKCS) && (pMechanism->mechanism != CKM_RSA_PKCS_OAEP))
		return CKR_MECHANISM_INVALID;
	

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_SignInit, hSession, pMechanism, hKey);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_Sign)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	if (pData == nullptr || ulDataLen < 0) 
		return CKR_ARGUMENTS_BAD;
	
	
	
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Sign, hSession, pData, ulDataLen, pSignature, pulSignatureLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));

	if (decodat == CKR_OK)
		if (pSignature == nullptr)
			memcpy(pulSignatureLen, raspuns + sizeof(CK_RV), sizeof(CK_ULONG));
		else
			memcpy(pSignature, raspuns + sizeof(CK_RV), *pulSignatureLen);

	delete[] raspuns;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_SignUpdate)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pPart, CK_ULONG ulPartLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (pPart == nullptr || ulPartLen < 0)
		return CKR_ARGUMENTS_BAD;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_SignUpdate, hSession, pPart, ulPartLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_SignFinal)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pSignature, CK_ULONG_PTR pulSignatureLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_SignFinal, hSession, pSignature, pulSignatureLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	
	if (decodat == CKR_OK)
		if (pSignature == nullptr)
			memcpy(pulSignatureLen, raspuns + sizeof(CK_RV), sizeof(CK_ULONG));
		else
			memcpy(pSignature, raspuns + sizeof(CK_RV), *pulSignatureLen);

	delete[] raspuns;
	return decodat;
}


