#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include "../denCoder/Encoder.h"
#include <WinSock2.h>


#include <set>
#include <list>
#include <map>

using namespace std;

extern bool isInited;

typedef struct CK_SEARCH_SESSION {
	list<CK_OBJECT_HANDLE>           hObjects;   /* list of objects for a session */
	list<CK_OBJECT_HANDLE>::iterator it;         /* current object */
}CK_SEARCH_SESSION;

static map<CK_SESSION_HANDLE, CK_SEARCH_SESSION> gSearchSessions;


CK_DEFINE_FUNCTION(CK_RV, C_FindObjectsInit)(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	//return CKR_OK; //test
	
	
	// check if another op is active
	map<CK_SESSION_HANDLE, CK_SEARCH_SESSION>::iterator itSessions = gSearchSessions.find(hSession);
	if (itSessions != gSearchSessions.end())
	{
		return CKR_OPERATION_ACTIVE;
	}

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_FindObjectsInit, hSession, pTemplate, ulCount);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat =CKR_OK;
	memcpy(&decodat, raspuns, sizeof(CK_RV));

	if (decodat == CKR_OK)
	{
		CK_SEARCH_SESSION thisSearchSession;
		CK_ULONG testcount = 0;
		memcpy(&testcount, raspuns + sizeof(CK_RV), sizeof(CK_ULONG));
		CK_ULONG offset = sizeof(CK_RV) + sizeof(CK_ULONG);
		for (CK_ULONG i = 0; i < testcount; i++)
		{
			CK_SESSION_HANDLE value;
			memcpy(&value, raspuns + offset, sizeof(CK_ULONG));
			thisSearchSession.hObjects.push_back(value);
			offset += sizeof(CK_SESSION_HANDLE);
		}
		// TODO: populate the hObjects list with the data from server
		// thisSearchSession.hObjecst = ...
		//thisSearchSession.it = thisSearchSession.hObjects.begin();

		gSearchSessions.insert(pair<CK_SESSION_HANDLE, CK_SEARCH_SESSION>(hSession, thisSearchSession));
		// add new search session 
		if (testcount != 0)
		{
			gSearchSessions.find(hSession)->second.it = gSearchSessions.find(hSession)->second.hObjects.begin();
		}
		else 
			gSearchSessions.find(hSession)->second.it = gSearchSessions.find(hSession)->second.hObjects.end();
	}
	delete[] raspuns;
	return decodat;
}

unsigned int ob = 0;
CK_DEFINE_FUNCTION(CK_RV, C_FindObjects)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE_PTR phObject, CK_ULONG ulMaxObjectCount, CK_ULONG_PTR pulObjectCount)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	
	
	// check if op is active
	map<CK_SESSION_HANDLE, CK_SEARCH_SESSION>::iterator itSessions = gSearchSessions.find(hSession);
	if (itSessions == gSearchSessions.end())
	{
		return CKR_OPERATION_NOT_INITIALIZED;
	}

	//CK_SEARCH_SESSION thisSearchSession = itSessions->second;

	*pulObjectCount = 0;
	while (itSessions->second.it != itSessions->second.hObjects.end() && ulMaxObjectCount > 0)
	{
		phObject[*pulObjectCount] = *(itSessions->second.it);
		(*pulObjectCount)++;
		ulMaxObjectCount--;
		++(itSessions->second.it);
	}

	return CKR_OK;

}

CK_DEFINE_FUNCTION(CK_RV, C_FindObjectsFinal)(CK_SESSION_HANDLE hSession)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	//return CKR_OK; //test
	
	// check if op is active
	map<CK_SESSION_HANDLE, CK_SEARCH_SESSION>::iterator itSessions = gSearchSessions.find(hSession);
	if (itSessions == gSearchSessions.end())
	{
		return CKR_OPERATION_NOT_INITIALIZED;
	}

	// clear the current list
	itSessions->second.hObjects.clear();

	// erase the search session
	gSearchSessions.erase(itSessions);

	return CKR_OK;
}



CK_DEFINE_FUNCTION(CK_RV, C_GetAttributeValue)(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_GetAttributeValue, hSession, hObject, pTemplate, ulCount);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat = CKR_OK;
	memcpy(&decodat, raspuns, sizeof(CK_RV));



	CK_ULONG attrCount = ulCount;
	
	CK_ULONG offset = sizeof(CK_RV);

	for (CK_ULONG iter = 0; iter < ulCount && decodat == CKR_OK; iter++)
	{

				if (pTemplate[iter].pValue != nullptr)
				{
					// if memory was allocated
					CK_ULONG lungime;
					memcpy(&lungime, raspuns + offset, sizeof(CK_ULONG));
					offset += sizeof(CK_ULONG);
					if (pTemplate[iter].ulValueLen >= lungime)
					{
						memcpy((pTemplate[iter].pValue), raspuns + offset, lungime);
					}
					else
					{
						//lRet = CKR_BUFFER_TOO_SMALL;
					}
					pTemplate[iter].ulValueLen = lungime;
				}
				else
				{
					memcpy(&(pTemplate[iter].ulValueLen), raspuns + offset, sizeof(CK_ULONG));
					offset += sizeof(CK_ULONG);
				}

	} // queried attr for
	delete[] raspuns;
	return decodat;
}