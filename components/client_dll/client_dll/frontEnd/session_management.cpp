#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include "../denCoder/Encoder.h"
#include <WinSock2.h>


unsigned int ATMSLOT = 1;
unsigned int nrSesiuni = 0;
unsigned int rws = 0;
extern bool isInited;




bool isLogged = false;
CK_DEFINE_FUNCTION(CK_RV, C_OpenSession)(CK_SLOT_ID slotID, CK_FLAGS flags, CK_VOID_PTR pApplication, CK_NOTIFY Notify, CK_SESSION_HANDLE_PTR phSession)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (slotID != ATMSLOT)
		return  CKR_SLOT_ID_INVALID;

	if (pApplication != NULL_PTR || Notify != NULL_PTR)
		return CKR_FUNCTION_FAILED;

	/* Verificare daca bitul CKF_SERIAL_SESSION e setat */
	if (!(flags & CKF_SERIAL_SESSION))
		return CKR_SESSION_PARALLEL_NOT_SUPPORTED;

	if (flags & ~(CKF_SERIAL_SESSION | CKF_RW_SESSION))
		return CKR_ARGUMENTS_BAD;



	/* Apel la service */
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_OpenSession, slotID, flags, pApplication, Notify, phSession);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	
	
	if (decodat == CKR_OK)
	{
		nrSesiuni++;
		if ((flags >> 2) % 2 == 1)
			rws++; 
		memcpy(phSession, raspuns + sizeof(CK_RV), sizeof(CK_SESSION_HANDLE)); //de fapt, valoarea din raspuns^^
	}
	/* Verificare primii 4 octeti (--ulong--) */
	/* Daca CKR_OK */
	/* *phSession = response_service_val + 4 ==session_handler*/
	/* else error: CKR_CRYPTOKI_NOT_INITIALIZED*/
	delete[] raspuns;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_CloseSession)(CK_SESSION_HANDLE hSession)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_CloseSession, hSession);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	if (decodat == CKR_OK)
	{
		nrSesiuni--;
		rws--; //aici trebuie testat ce tip de sesiune era..poate o lista cu sesiunile rw? setate in opensession si apoi find pe ea
	}
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_CloseAllSessions)(CK_SLOT_ID slotID)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_CloseAllSessions, slotID);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	if (decodat == CKR_OK)
	{
		rws = 0; 
		nrSesiuni = 0;
	}
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetSessionInfo)(CK_SESSION_HANDLE hSession, CK_SESSION_INFO_PTR pInfo)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (pInfo == NULL_PTR)
		return CKR_ARGUMENTS_BAD;

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_GetSessionInfo, hSession, pInfo);


	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));

	if (decodat == CKR_OK)
	{
		pInfo->slotID = ATMSLOT;
		memcpy(&(pInfo->state), raspuns + sizeof(CK_RV), sizeof(CK_STATE));
		memcpy(&(pInfo->flags), raspuns + sizeof(CK_RV) + sizeof(CK_STATE), sizeof(CK_FLAGS));
		pInfo->ulDeviceError = 0;
	}
	delete[] raspuns;
	return decodat;
}




CK_DEFINE_FUNCTION(CK_RV, C_Login)(CK_SESSION_HANDLE hSession, CK_USER_TYPE userType, CK_UTF8CHAR_PTR pPin, CK_ULONG ulPinLen)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	/* pPin = NULL_PTR -->CKF_PROTECTED_AUTHENTICATION_PATH flag set */
	if (pPin == NULL_PTR && ulPinLen > 0)
		return CKR_ARGUMENTS_BAD;

	/* CKR_PIN_INCORRECT */
	/* or CKR_OK and */

	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Login, hSession, userType, pPin, ulPinLen);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	if (decodat == CKR_OK)
		isLogged = true;
	return decodat;
}


CK_DEFINE_FUNCTION(CK_RV, C_Logout)(CK_SESSION_HANDLE hSession)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	/* not logged in */
	if (!isLogged)
		return CKR_USER_NOT_LOGGED_IN;

	/* service call  */
	unsigned long encoded = 0;
	unsigned char * comanda = Encoder::gInstance(4)->encode(&encoded, En_C_Logout, hSession);

	int nr;
	unsigned char * raspuns = DLLClient::gInstance()->gResult(comanda, encoded, &nr);
	free(comanda);

	CK_RV decodat;
	memcpy(&decodat, raspuns, sizeof(CK_RV));
	delete[] raspuns;

	if (decodat == CKR_OK)
		isLogged = !isLogged;

	return decodat;
}
