#pragma once
#include "pkcs11.h"
#include "../DllClient/DLLClient.h"
#include <WinSock2.h>

extern bool isInited;
extern unsigned int ATMSLOT;
extern unsigned int nrSesiuni;
extern unsigned int rws;
CK_DEFINE_FUNCTION(CK_RV, C_GetSlotList)(CK_BBOOL tokenPresent, CK_SLOT_ID_PTR pSlotList, CK_ULONG_PTR pulCount)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;

	*pulCount = 1;
	if (pSlotList != nullptr)
		pSlotList[0] = (CK_ULONG)ATMSLOT;
	
	return CKR_OK;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetSlotInfo)(CK_SLOT_ID slotID, CK_SLOT_INFO_PTR pInfo)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (slotID != ATMSLOT)
		return CKR_SLOT_ID_INVALID;

	if (pInfo != nullptr)
	{
		memset(pInfo->manufacturerID, ' ', 32);
		memset(pInfo->slotDescription, ' ', 64);
		unsigned char mda[] = "ATM@Registered";
		unsigned char mda2[] = "ATM_Slots_Management";
		memcpy(pInfo->manufacturerID, mda, 14);
		memcpy(pInfo->slotDescription, mda2, 20);

		// #define CKF_TOKEN_PRESENT     0x00000001UL  /* a token is there */
		// #define CKF_REMOVABLE_DEVICE  0x00000002UL  /* removable devices*/
		// #define CKF_HW_SLOT           0x00000004UL  /* hardware slot */

		pInfo->flags = CKF_TOKEN_PRESENT | CKF_HW_SLOT | CKF_REMOVABLE_DEVICE;

		//pt CK_VERSION
		//versiune		major		minor
		//  1.0			 0x01		 0x00
		//  2.10		 0x02		 0x0a
		//  2.11		 0x02		 0x0b

		pInfo->hardwareVersion.major = 0x02;
		pInfo->hardwareVersion.minor = 0x28;

		pInfo->firmwareVersion.major = 0x02;
		pInfo->firmwareVersion.minor = 0x28;

		return CKR_OK;
	}
	return CKR_HOST_MEMORY;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetTokenInfo)(CK_SLOT_ID slotID, CK_TOKEN_INFO_PTR pInfo)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (slotID != ATMSLOT)
		return CKR_SLOT_ID_INVALID;


	if (pInfo != nullptr)
	{

		memset(pInfo->manufacturerID, ' ', 32);
		memset(pInfo->serialNumber, ' ', 32);
		memset(pInfo->model, ' ', 32);
		memset(pInfo->label, ' ', 64);
		unsigned char mda[] = "ATM@Registered";
		unsigned char mda2[] = "ATM_token";
		unsigned char mda3[] = "ATM0.1";
		unsigned char mda4[] = "123413";
		memcpy(pInfo->manufacturerID, mda, 14);
		memcpy(pInfo->label, mda2, 9);
		memcpy(pInfo->model, mda3, 6);
		memcpy(pInfo->serialNumber, mda4, 6);

		// #define CKF_RNG                     0x00000001UL  /* has random # generator */
		// #define CKF_WRITE_PROTECTED         0x00000002UL  /* token is write-protected */
		// #define CKF_LOGIN_REQUIRED          0x00000004UL  /* user must login */
		// #define CKF_USER_PIN_INITIALIZED    0x00000008UL  /* normal user's PIN is set */

		pInfo->flags = CKF_LOGIN_REQUIRED | CKF_USER_PIN_INITIALIZED | CKF_TOKEN_INITIALIZED;

		pInfo->ulMaxSessionCount = (CK_ULONG)0;
		pInfo->ulSessionCount = (CK_ULONG)nrSesiuni; //aici ar trebui luat de la svr

		pInfo->ulMaxRwSessionCount = (CK_ULONG)0;
		pInfo->ulRwSessionCount = (CK_ULONG)rws; //aici ar trebui luat de la svr

		pInfo->ulMaxPinLen = (CK_ULONG)24;
		pInfo->ulMinPinLen = (CK_ULONG)6;

		pInfo->ulTotalPublicMemory = (CK_ULONG)81920;
		pInfo->ulFreePublicMemory = (CK_ULONG)32767;
		pInfo->ulTotalPrivateMemory = (CK_ULONG)81920;
		pInfo->ulFreePrivateMemory = (CK_ULONG)32767;

		pInfo->hardwareVersion.major = 0x02;
		pInfo->hardwareVersion.minor = 0x00;

		pInfo->firmwareVersion.major = 0x02;
		pInfo->firmwareVersion.minor = 0x00;

		
		//strcpy((char*)pInfo->utcTime, 0x00); //ar trebui hex?

		return CKR_OK;
	}
	else
		return CKR_ARGUMENTS_BAD;

}

CK_DEFINE_FUNCTION(CK_RV, C_GetMechanismList)(CK_SLOT_ID slotID, CK_MECHANISM_TYPE_PTR pMechanismList, CK_ULONG_PTR pulCount)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	if (slotID != ATMSLOT)
		return CKR_SLOT_ID_INVALID;
	if (pMechanismList == NULL_PTR)
	{
		//daca lista e nula, returnam in pulCount numarul de mecanisme, fara a returna o lista

		//apelez TICA TICA TICA
		if (true) //adica apelul la tica a reusit
		{
			//nu mai are rost sa fac free

			*pulCount = 10;
			return CKR_OK;
		}
		else
		{
			return CKR_FUNCTION_FAILED;
		}


	}
	else
	{
		CK_MECHANISM_TYPE_PTR testList = (CK_MECHANISM_TYPE_PTR)malloc(10 * sizeof(CK_MECHANISM_TYPE));
		//aici verific daca coincid valorile primite de la tica
		//sa fie bufferul destul de mare
		CK_ULONG testVal = 10;

		if (sizeof(*testList) >= testVal * sizeof(CK_MECHANISM_TYPE))
		{
			//apelez TICA si obtin lista de lista de mechanisme
			pMechanismList = testList;


			//plus, trebuie sa setam pulCount



			//apel TICA pt marimea corecta
			*pulCount = 10;
			//val de teste
		}
		else
		{
			//returnam CKR-buf-to-small
			//plus, trebuie sa setam pulCount

			//apel TICA pt marimea corecta
			*pulCount = 10;
			//val de teste


			return CKR_BUFFER_TOO_SMALL;
		}
	}

	return CKR_OK;
}


CK_DEFINE_FUNCTION(CK_RV, C_GetMechanismInfo)(CK_SLOT_ID slotID, CK_MECHANISM_TYPE type, CK_MECHANISM_INFO_PTR pInfo)
{
	if (isInited == false)
		return CKR_CRYPTOKI_NOT_INITIALIZED;
	//obtine informatii despre un mecanism particular, posibil suportat de un token

	//slotID	is the ID of the token's slot;
	//type	is the type of mechanism;
	//pInfo	points to the location that receives the mechanism information.
	if (slotID != NULL)
	{
		//avem un slotID valid
		//testam sa avem si un type valid
		if (type != NULL)
		{
			//avem type valid
			//cerem info la TICA
			if (pInfo != nullptr)
			{
				return CKR_OK;
			}
			else
			{
				return CKR_ARGUMENTS_BAD;
			}

			//verifica marime pinfo
		}
		else
			return CKR_ARGUMENTS_BAD;
	}
	else
		return CKR_ARGUMENTS_BAD;
}

