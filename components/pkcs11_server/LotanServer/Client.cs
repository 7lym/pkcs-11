﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    //client class
    public class Client
    {
        System.Object this_lock = new System.Object();// mutex folosit in context multithreading
        private int sessionNr = 1;
        public byte[] identifier = null;

        /// <summary>Functie ce inchide o sesiune.
        /// <para>Int ce reprezinta id-ul sesiunii.</para>
        /// </summary>
        public Rezult CloseSession(int id)
        {
            lock (this_lock)//lock context
            {
                if (!sessions.ContainsKey(id))
                    return new Rezult(CK_ERRORS.CK_BAD_SESSION);
                else
                {
                    sessions.Remove(id);
                    return new Rezult(0);
                }
            }
        }
        /// <summary>Inchiderea tuturor sesiunilor aflate pe un client.       
        /// </summary>
        public void CloseAllConexions()
        {
            {
                lock (this_lock)//lock context
                {
                    sessions.Clear();
                }
            }
        }

        /// <summary>Creare sesiune noua.
        /// </summary>
        public int GetHSession()
        {
            lock (this_lock)//lock context
            {
                sessionNr++;
                Session s = new Session();//creare sesiune noua
                s.HSessionID = sessionNr;
                sessions.Add(sessionNr, s);//adaugare sesiune
                return sessionNr;
            }
        }
        public Dictionary<int, Session> sessions = new Dictionary<int, Session>();//folosim dictionar pentru a stoca sesiunile => hastable cu acces o(1)
        public bool authentificated = false;//login state
        public int userType = 0;//user type
        
    }

    public class Session
    {
        public int DataBaseKey = -1;// intrarea din baza de date
        public Int64 HSessionID = 0;//id de sesiune
        //public byte[] HSession = null;
        public Int64 FID = 0;//id-ul functie
        public byte[] cryptoarg = null;//argumentul primit ca parametru de la client
        public int mid = 0;//id-ul mecanizmului pkcs 11
        public List<byte> messagePump = new List<byte>();// <concept de message Pump> stocarea rezultatului pana la urmatorul request, in conditii de functii update
        public string tempFile = "";//fisier temporar pentru functiile update
        public int sessionFlags = 0;// flag-uri setate pentru sesiune
        //public IFunction executor = null;


        //obsolete
        public void SetScope(byte[] data)
        {

        }
    }

    //clasa ce face managementul clientilor
    public class ClientManager
    {
        HashSet<string> tempFiles = new HashSet<string>();//hastable pentru fisiere temporare --sa nu duplicam/utilizam un fisier existent
        System.Object this_lock = new System.Object();//mutex
        Dictionary<string, Client> container = new Dictionary<string, Client>();//dictionar pentru clientii existenti
        /// <summary>Cautare thread-safe de clienti.
        /// <para>Int ce reprezinta id-ul clientului.</para>
        /// </summary>
        public bool containsID(string id)
        {
            lock (this_lock)//lock context
            {
                return container.ContainsKey(id);
            }
        }
        /// <summary>Obtinere referinta catre obiectul de tip client thread-safe.
        /// <para>Int ce reprezinta id-ul clientului.</para>
        /// </summary>
        public Client GetClient(string id)
        {
            lock (this_lock)
            {
                return container[id];
            }
        }
        /// <summary>Adaugare client thread-safe.
        /// <para>Int ce reprezinta id-ul clientului.</para>
        /// </summary>
        public void addClient(string id)
        {
            lock (this_lock)
            {
                container.Add(id, new Client());
                container[id].identifier = Utils.ConvertStringToBytes(id);
            }
        }

        /// <summary>Stergere client thread-safe.
        /// <para>Int ce reprezinta id-ul clientului.</para>
        /// </summary>
        public void deleteClient(string id)
        {
            lock (this_lock)
            {
                container.Remove(id);
            }
        }

        /// <summary>Obtinere fisier temporar.
        /// </summary>
        public string GetTempFile()
        {
            lock (this_lock)
            {
                Random r = new Random();
                int value = 0;
                while (true)
                {
                    value = r.Next(0, int.MaxValue);
                    if (!tempFiles.Contains(value.ToString()))//cream nume temporar random--utilizam cele existente
                    {
                        tempFiles.Add(value.ToString());
                        return value.ToString();
                    }
                }
            }
        }
    }
}
