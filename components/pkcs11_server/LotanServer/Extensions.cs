﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    //extindem functionalitatile Array
    public static class Extensions
    {
        /// <summary>Obtine subset din set --subsir din sir.
        /// <para>data-referinta catre sir,index- indexul de inceput, inclusiv, lenght--cate elemente sa se copieze.</para>
        /// </summary>
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        /// <summary>Obtine subset din set --subsir din sir--reverse order.
        /// <para>data-referinta catre sir,index- indexul de inceput, inclusiv, lenght--cate elemente sa se copieze.</para>
        /// </summary>
        public static T[] ReverseSubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            Array.Reverse(result);
            return result;
        }
    }
}
