﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    //define -- coduri de eroare locale pentru server
    public class CK_ERRORS
    {
        public const int CK_INTERNAL_ERROR = 1;
        public const int CK_NOT_SUPPORTED = 2;
        public const int CK_UNRECOGNIZED_PACKET = 3;
        public const int CK_UNABLE_TO_PROCESS_PACKET = 4;
        public const int CK_UNRECOGNIZED_CHANEL = 5;
        public const int CK_UNRECOGNIZED_FUNCTION = 6;
        public const int CK_TRUE = 0;
        public const int CK_FALSE = 8;
        public const int CK_BAD_ARGUMENT = 9;
        public const int CK_BAD_SESSION = 11;
        public const int CK_MECANISM_NOT_SUPPORTED = 12;
        public const int CK_PKCS_UNRECOGNIZED_ENTRY = 13;
        public const int CK_CRYPTOGRAPHIC_ALGORITHM_ERROR = 14;
        public const int CKR_PIN_INCORRECT = 0x000000a0;
    }

    //definire coduri de mecanisme
    public class CK_MECANISM
    {
        /// <summary>Hashtable ce contine mecanismele suportate.
        /// <para>Orice nou mecanism trebuie adaugat aici.</para>
        /// </summary>
        public static List<int> MecanismList
        {
            get
            {
                List<int> list = new List<int>();
                list.Add(0x00000250);//sha256
                list.Add(0x00000255);//sha224
                list.Add(0x00000260);//sha384
                list.Add(0x00000270);//sha512
                list.Add(0x00000001);//nu stiu ce e
                return list;
            }
        }
        public static List<int> SHA_VALUES
        {
            get
            {
                List<int> list = new List<int>();
                int[] range = { 0x00000250, 0x00000255, 0x00000260, 0x00000270 };
                list.AddRange(range);
                return list;
            }
        }
        public const int CKF_HW = 0x00000001;
        public const int CKF_ENCRYPT = 0x00000100;
        public const int CKF_DECRYPT = 0x00000200;
        public const int CKF_DIGEST = 0x00000400;
        public const int CKF_SIGN = 0x00000800;

        //define -- codurile utilizate pentru sha
        public class CK_SHA
        {
            public const int SHA256 = 0x00000250;
            public const int SHA224 = 0x00000255;
            public const int SHA384 = 0x00000260;
            public const int SHA512 = 0x00000270;
            //public int[] SHA_VALUES ={};
        }

        /// <summary>Obtine flag-uri pentru mecanismul specificat.
        /// <para>Tipul mecanismului.</para>
        /// </summary>
        public static int GetFlags(int mecanism)
        {
            if (mecanism >= 0x00000250 && mecanism <= 0x00000270)
                return CKF_HW & CKF_DIGEST;
            else
                return 0;
        }
    }

    //define id-uri pentru state-ul sesiunii
    public class CK_SESSION_INFO
    {
        public const int CKS_RO_PUBLIC_SESSION = 0;
        public const int CKS_RO_USER_FUNCTIONS = 1;
        public const int CKS_RW_PUBLIC_SESSION = 2;
        public const int CKS_RW_USER_FUNCTIONS = 3;
        public const int CKS_RW_SO_FUNCTIONS = 4;
        public const int CKF_RW_SESSION = 0x00000002;
        public const int CKF_SERIAL_SESSION = 0x00000004;
        /// <summary>Obtinere informatii cu privire la state-ul sesiunii.
        /// <para>.</para>
        /// </summary>
        public static int GetSessionInfoState(int sid, Client cl)
        {
            if (!cl.authentificated)
                return CKS_RW_PUBLIC_SESSION;
            else
                return CKS_RW_USER_FUNCTIONS;
        }
        public static int GetSessionInfoFlags(int sid, Client cl)
        {
            return cl.sessions[sid].sessionFlags;
            //if (!cl.authentificated)
            //    return CKF_SERIAL_SESSION;
            //else
            //    return CKF_SERIAL_SESSION&CKF_RW_SESSION;
        }
    }

    public class CKA
    {
        public const int CKA_CLASS = 0;
        public const int CKA_TOKEN = 1;
        public const int CKA_PRIVATE = 2;
        public const int CKA_OBJECT_ID = 3;
        public const int CKA_CERTIFICATE_TYPE = 4;
        public const int CKA_KEY_TYPE = 5;
        public const int CKA_SUBJECT = 6;
        public const int CKA_ENCRYPT = 7;
        public const int CKA_DECRYPT = 8;
        public const int CKA_SIGN = 9;
        public const int CKA_VALUE = 10;
        public const int CKA_JUST_FOR_AUTH_SESSION = 100;
        public static List<Tuple<int, string>> DeserializeTask(List<Tuple<int, byte[]>> attr)
        {
            List<Tuple<int, string>> list = new List<Tuple<int, string>>();

            foreach (var atr in attr)
            {
                switch (atr.Item1)
                {
                    case 0x00000000:
                        list.Add(new Tuple<int, string>(CKA.CKA_CLASS, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000001:
                        list.Add(new Tuple<int, string>(CKA.CKA_TOKEN, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000002:
                        list.Add(new Tuple<int, string>(CKA.CKA_PRIVATE, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000102:
                        list.Add(new Tuple<int, string>(CKA.CKA_OBJECT_ID, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000080:
                        list.Add(new Tuple<int, string>(CKA.CKA_CERTIFICATE_TYPE, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000100:
                        list.Add(new Tuple<int, string>(CKA.CKA_KEY_TYPE, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000101:
                        list.Add(new Tuple<int, string>(CKA.CKA_SUBJECT, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000104:
                        list.Add(new Tuple<int, string>(CKA.CKA_ENCRYPT, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000105:
                        list.Add(new Tuple<int, string>(CKA.CKA_DECRYPT, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000108:
                        list.Add(new Tuple<int, string>(CKA.CKA_SIGN, Convert.ToBase64String(atr.Item2)));
                        break;
                    case 0x00000011:
                        list.Add(new Tuple<int, string>(CKA.CKA_VALUE, Convert.ToBase64String(atr.Item2)));
                        break;
                    default:
                        throw new Exception("Atribute not found");
                }
            }
            return list;
        }
    }
    public class Atribute
    {
        public int CKA_ID = -1;
        public bool objectAuthState = false;
        private List<string> Atributes = new List<string>();
        public Atribute(DataRow dr, int nrCols)
        {
            CKA_ID = Convert.ToInt32(dr[0]);
            for (int i = 1; i < nrCols - 1; i++)
                Atributes.Add(dr[i].ToString());
            objectAuthState = Convert.ToInt16(dr[nrCols - 1]) > 0 ? true : false;
        }
        public bool ValidateTemplate(List<Tuple<int, string>> template)
        {
            foreach (var poz in template)
                if (Atributes[poz.Item1] != poz.Item2)
                    return false;
            return true;
        }
        public byte[] GetAttributeValue(List<Tuple<int, string>> template)
        {
            List<byte> val = new List<byte>();
            foreach (var poz in template)
            {
                byte[] decoded = Convert.FromBase64String(Atributes[poz.Item1]);
                if (poz.Item2.Length < 4)
                {
                    val.AddRange(Utils.ConvertIntToByteArray(decoded.Length));
                }
                else
                {
                    val.AddRange(Utils.ConvertIntToByteArray(decoded.Length));
                    val.AddRange(decoded);
                }
            }
            return val.ToArray();
        }
    }
    static public class Functions
    {
        public static bool CheckChanel(string key)
        {
            return Global.manager.containsID(key);
        }
        public static Rezult RootFunction(SslStream sslStream, byte[] data, string key)
        {
            int fid = Convert.ToInt32(data[0]);
            Console.WriteLine("In function " + fid);
            data = data.SubArray(1, data.Length - 1);
            if (fid <= 0 || fid > Global.limit)
                return Rezult.ReturnError(CK_ERRORS.CK_UNRECOGNIZED_FUNCTION);
            else
            {
                if (Global.BaseFunctions.Contains(fid))
                {
                    return BaseFunctions(sslStream, data, key, fid);
                }
                else
                {
                    return SessionFunctions(sslStream, data, key, fid);
                }
            }

        }

        public static Rezult BaseFunctions(SslStream sslStream, byte[] data, string key, int fid)
        {
            switch (fid)
            {
                case 1:
                    return new Rezult { data = data, error = 0 };
                case 2:
                    {
                        //if (data.Length != 8)
                        //    return new Rezult(CK_ERRORS.CK_BAD_ARGUMENT);
                        //if (Utils.ConvertBytesToLong(data)!=0)
                        //    return new Rezult(CK_ERRORS.CK_BAD_ARGUMENT);
                        Global.manager.deleteClient(key);
                        //sslStream.WriteByte();
                        byte[] d = new byte[1];
                        d[0] = Convert.ToByte(CK_ERRORS.CK_TRUE);

                        return new Rezult { data = d };
                    }
                case 7:
                    {
                        if (Utils.ConvertBytesToInt(data.SubArray(0, 4)) == 0)
                            return new Rezult { data = Utils.ConvertIntToByteArray(CK_MECANISM.MecanismList.Count) };
                        return new Rezult { data = Utils.AddLenghtByteChunk(Utils.ConvertListIntegerToBytes(CK_MECANISM.MecanismList)) };
                    }
                case 8:
                    {
                        List<byte> list = new List<byte>();
                        list.AddRange(Utils.ConvertIntToByteArray(16));
                        list.AddRange(Utils.ConvertIntToByteArray(32));
                        list.AddRange(Utils.ConvertIntToByteArray(CK_MECANISM.GetFlags(Utils.ConvertBytesToInt(data.SubArray(0, 4)))));
                        return new Rezult { data = list.ToArray() };
                    }
                case 9:
                    {
                        List<byte> response = new List<byte>();
                        response.Add(Convert.ToByte(0));
                        return new Rezult { data = response.ToArray() };



                        byte[] antet = Utils.ConvertStringToBytes("default");
                        response.AddRange(antet);
                        response.AddRange(Utils.BytePadding(32 - antet.Length));
                        response.AddRange(antet);
                        response.AddRange(Utils.BytePadding(32 - antet.Length));
                        response.AddRange(antet);
                        response.AddRange(Utils.BytePadding(16 - antet.Length));
                        response.AddRange(antet);
                        response.AddRange(Utils.BytePadding(16 - antet.Length));
                        Int64 val = 0;
                        response.AddRange(Utils.ConvertIntToByteArray(val));
                        val = int.MaxValue;
                        response.AddRange(Utils.ConvertIntToByteArray(val));
                        val = Global.manager.GetClient(key).sessions.Count;
                        response.AddRange(Utils.ConvertIntToByteArray(val));
                        val = 0;
                        for (int i = 0; i < 10; i++)
                            response.AddRange(Utils.ConvertIntToByteArray(val));
                        return new Rezult { data = response.ToArray() };
                    }
                case 10:
                    {
                        int sid = Global.manager.GetClient(key).GetHSession();
                        Global.manager.GetClient(key).sessions[sid].sessionFlags = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                        return new Rezult { data = Utils.ConvertIntToByteArray(sid) };
                    }
                case 11:
                    {
                        if (data.Length != 4)
                            return new Rezult(Convert.ToByte(CK_ERRORS.CK_BAD_ARGUMENT));

                        Rezult r = Global.manager.GetClient(key).CloseSession(Utils.ConvertBytesToInt(data));
                        if (!r.AreErrors)
                            return new Rezult(Convert.ToByte(CK_ERRORS.CK_TRUE));
                        return r;
                    }
                case 12:
                    {
                        Global.manager.GetClient(key).CloseAllConexions();
                        return new Rezult(Convert.ToByte(CK_ERRORS.CK_TRUE));
                    }
                case 13:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                List<byte> list = new List<byte>();
                                list.AddRange(Utils.ConvertIntToByteArray(CK_SESSION_INFO.GetSessionInfoState(sid, cl)));
                                list.AddRange(Utils.ConvertIntToByteArray(CK_SESSION_INFO.GetSessionInfoFlags(sid, cl)));
                                return new Rezult { data = list.ToArray() };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 14:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));

                            if (cl.sessions.ContainsKey(sid))
                            {
                                int userType = Utils.ConvertBytesToInt(data.SubArray(4, 4));
                                int lenght = Utils.ConvertBytesToInt(data.SubArray(8, 4));
                                data = data.SubArray(12, data.Length - 12);
                                if (data.Length != lenght)
                                    return new Rezult { error = CK_ERRORS.CK_BAD_ARGUMENT };
                                else
                                {
                                    string pin = Utils.ConvertByteToString(data);
                                    try
                                    {
                                        DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_Authentificate", 0, new SqlParameter { ParameterName = "@PIN", SqlDbType = SqlDbType.NVarChar, Size = 100, Value = pin });
                                        if (ds.Tables[0].Rows.Count == 1)
                                        {
                                            cl.sessions[sid].DataBaseKey = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                                            cl.authentificated = true;
                                            cl.userType = userType;
                                            return new Rezult { data = Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE) };
                                        }
                                        else
                                            return new Rezult { error = CK_ERRORS.CKR_PIN_INCORRECT };
                                    }
                                    catch (Exception ex)
                                    {
                                        return new Rezult { error = CK_ERRORS.CK_INTERNAL_ERROR };
                                    }
                                }
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 15:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                cl.authentificated = false;
                                return new Rezult { data = Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE) };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                default:
                    return new Rezult(CK_ERRORS.CK_NOT_SUPPORTED);
            }
            //return new Rezult();
        }
        public static Rezult SessionFunctions(SslStream sslStream, byte[] data, string key, int fid)
        {
            switch (fid)
            {
                case 17:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));

                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int nrAttr = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                //data = data.SubArray(4, data.Length - 4);
                                //int nr=Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);
                                List<Tuple<int, byte[]>> template = new List<Tuple<int, byte[]>>();

                                if (data.Length <= 8)
                                    return new Rezult { error = CK_ERRORS.CK_BAD_ARGUMENT };
                                while (true)
                                {
                                    int code = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                    int slen = Utils.ConvertBytesToInt(data.SubArray(4, 4));
                                    template.Add(new Tuple<int, byte[]>(code, data.SubArray(8, slen)));
                                    data = data.SubArray(8 + slen, data.Length - 8 - slen);
                                    if (data.Length <= 8)
                                        break;

                                }
                                List<Tuple<int, string>> _template = CKA.DeserializeTask(template);
                                DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_GetAtribute", 0);
                                List<byte> atributes = new List<byte>();
                                int nrOfAttr = 0;
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    Atribute attr = new Atribute(dr, ds.Tables[0].Columns.Count);
                                    if (attr.ValidateTemplate(_template) && ((attr.objectAuthState && cl.authentificated) || !attr.objectAuthState))
                                    {
                                        atributes.AddRange(Utils.ConvertIntToByteArray(attr.CKA_ID));
                                        nrOfAttr++;
                                    }
                                }
                                Console.WriteLine("Nr of atributes found " + nrOfAttr);
                                List<byte> combine = new List<byte>();
                                combine.AddRange(Utils.ConvertIntToByteArray(nrOfAttr));
                                combine.AddRange(atributes);
                                atributes = combine;
                                return new Rezult { data = atributes.ToArray() };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 20:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int nr = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);
                                DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_GetAtributeValues", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = nr });
                                nr = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);

                                List<Tuple<int, byte[]>> template = new List<Tuple<int, byte[]>>();

                                if (data.Length < 8)
                                    return new Rezult { error = CK_ERRORS.CK_BAD_ARGUMENT };
                                while (true)
                                {
                                    int code = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                    int slen = Utils.ConvertBytesToInt(data.SubArray(4, 4));
                                    template.Add(new Tuple<int, byte[]>(code, data.SubArray(8, slen)));
                                    data = data.SubArray(8 + slen, data.Length - 8 - slen);
                                    if (data.Length <= 8)
                                        break;

                                }
                                List<Tuple<int, string>> _template = CKA.DeserializeTask(template);

                                List<byte> allData = new List<byte>();
                                if (ds.Tables[0].Rows.Count == 1)
                                {
                                    Atribute atr = new Atribute(ds.Tables[0].Rows[0], ds.Tables[0].Columns.Count);
                                    allData.AddRange(atr.GetAttributeValue(_template));
                                }
                                return new Rezult { data = allData.ToArray() };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 25:
                case 29:
                case 33:
                case 21:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int mecan = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);
                                if (CK_MECANISM.MecanismList.Contains(mecan))
                                {
                                    int l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                    cl.sessions[sid].mid = mecan;
                                    data = data.SubArray(4, data.Length - 4 + l);

                                    if (data.Length > 0)
                                    {
                                        cl.sessions[sid].cryptoarg = data.SubArray(0, 4);

                                        return new Rezult { data = Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE) };
                                        //if (Utils.ConvertBytesToInt(data) == cl.sessions[sid].DataBaseKey)
                                        //{
                                        //    cl.sessions[sid].cryptoarg = data.SubArray(0, 4);
                                        //    return new Rezult { data = Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE) };
                                        //}
                                        //else
                                        //    return new Rezult { error = CK_ERRORS.CK_PKCS_UNRECOGNIZED_ENTRY };

                                    }
                                    else
                                    {
                                        if (!CK_MECANISM.SHA_VALUES.Contains(mecan))
                                            return new Rezult { error = CK_ERRORS.CK_BAD_ARGUMENT };
                                        cl.sessions[sid].cryptoarg = new byte[0];

                                        return new Rezult { data = Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE) };
                                    }
                                }
                                else
                                    return new Rezult { error = CK_ERRORS.CK_MECANISM_NOT_SUPPORTED };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 30:
                case 22:
                case 26:
                case 34:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);
                                byte[] bufferedData = data.SubArray(0, l);
                                data = data.SubArray(l, data.Length - l);
                                l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                data = data.SubArray(4, data.Length - 4);
                                byte[] response = null;
                                if (l != 0&& cl.sessions[sid].messagePump.Count>0)
                                {
                                    var r= new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    cl.sessions[sid].messagePump.Clear();
                                    return r;
                                }
                                else
                                    cl.sessions[sid].messagePump = new List<byte>();
                                switch (fid)
                                {
                                    case 34:
                                        {
                                            response = SHAManager.InMemoryHash(bufferedData, cl.sessions[sid].mid);
                                            if (response == null)
                                                return new Rezult { error = CK_ERRORS.CK_CRYPTOGRAPHIC_ALGORITHM_ERROR };
                                            cl.sessions[sid].messagePump.Clear();
                                            //cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 22:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });

                                            int keysize = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][1].ToString();
                                            response = RSAManager.InMemoryRSA(cl.sessions[sid].mid, bufferedData, keyxml, keysize);
                                            if (response == null)
                                                return new Rezult { error = CK_ERRORS.CK_CRYPTOGRAPHIC_ALGORITHM_ERROR };
                                            break;
                                        }
                                    case 26:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });

                                            int keysize = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][1].ToString();
                                            response = RSAManager.InMemoryRSADecrypt(cl.sessions[sid].mid, bufferedData, keyxml, keysize);
                                            if (response == null)
                                                return new Rezult { error = CK_ERRORS.CK_CRYPTOGRAPHIC_ALGORITHM_ERROR };

                                            break;
                                        }
                                    case 30:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][1].ToString();
                                            response = RSAManager.InMemorySign(keyxml, bufferedData, cl.sessions[sid].mid);
                                            if (response == null)
                                                return new Rezult { error = CK_ERRORS.CK_CRYPTOGRAPHIC_ALGORITHM_ERROR };
                                            //return new Rezult { data = response };
                                            break;
                                        }
                                    default:
                                        return new Rezult(CK_ERRORS.CK_NOT_SUPPORTED);
                                }
                                if (response == null)
                                    return new Rezult { error = CK_ERRORS.CK_NOT_SUPPORTED };
                                else
                                {
                                    cl.sessions[sid].messagePump.AddRange(response);
                                    return new Rezult { data = Utils.ConvertIntToByteArray(response.Length) };
                                }
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 23:
                case 27:
                case 35:
                case 31:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                int l2 = Utils.ConvertBytesToInt(data.SubArray(l, 4));
                                data = data.SubArray(4, l);
                                if (cl.sessions[sid].tempFile == "")
                                {
                                    cl.sessions[sid].tempFile = Global.manager.GetTempFile();
                                    System.IO.File.WriteAllText(cl.sessions[sid].tempFile, string.Empty);
                                }
                                if (l2 > 0&&fid!=35 && cl.sessions[sid].messagePump.Count > 0)
                                {
                                    //return new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    var r = new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    cl.sessions[sid].messagePump.Clear();
                                    return r;
                                }


                                byte[] response = null;
                                Utils.AppendAllBytes(cl.sessions[sid].tempFile, data);
                                switch (fid)
                                {
                                    case 35:
                                        {
                                            response = SHAManager.InMemoryHash(data, cl.sessions[sid].mid);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 23:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.InMemoryRSA(cl.sessions[sid].mid, data, keyxml, keysize);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 27:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.InMemoryRSADecrypt(cl.sessions[sid].mid, data, keyxml, keysize);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 31:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.InMemorySign(keyxml, data, cl.sessions[sid].mid);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    default:
                                        return new Rezult(CK_ERRORS.CK_NOT_SUPPORTED);
                                }


                                if (response == null)
                                    return new Rezult { error = CK_ERRORS.CK_NOT_SUPPORTED };
                                else
                                    return new Rezult { data = Utils.ConvertIntToByteArray(response.Length) };


                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 36:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                

                                if (l > 0 && cl.sessions[sid].messagePump.Count > 0)
                                {
                                    var r = new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    cl.sessions[sid].messagePump.Clear();
                                    return r;
                                }
                                byte[] response = null;

                                response = SHAManager.OnHardHash(cl.sessions[sid].tempFile, cl.sessions[sid].mid);//   InMemoryHash(data, cl.sessions[sid].mid);
                                cl.sessions[sid].messagePump.Clear();
                                cl.sessions[sid].messagePump.AddRange(response);


                                if (response == null)
                                    return new Rezult { error = CK_ERRORS.CK_NOT_SUPPORTED };
                                else
                                    return new Rezult { data = Utils.ConvertIntToByteArray(response.Length) };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }
                case 32:
                case 28:
                case 24:
                    {
                        if (Global.manager.containsID(key))
                        {
                            Client cl = Global.manager.GetClient(key);
                            int sid = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                            if (cl.sessions.ContainsKey(sid))
                            {
                                data = data.SubArray(4, data.Length - 4);
                                int l = Utils.ConvertBytesToInt(data.SubArray(0, 4));
                                int l2 = Utils.ConvertBytesToInt(data.SubArray(l, 4));
                                data = data.SubArray(4, l);
                                if (l2 > 0&&fid!=35 && cl.sessions[sid].messagePump.Count > 0)
                                {
                                    //return new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    var r = new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    cl.sessions[sid].messagePump.Clear();
                                    return r;
                                }

                                if (l>0&& cl.sessions[sid].messagePump.Count>0)
                                {
                                    var r = new Rezult { data = cl.sessions[sid].messagePump.ToArray() };
                                    cl.sessions[sid].messagePump.Clear();
                                    return r;
                                }
                                byte[] response = null;

                                switch (fid)
                                {
                                    case 36:
                                        {
                                            response = SHAManager.OnHardHash(cl.sessions[sid].tempFile, cl.sessions[sid].mid);//   InMemoryHash(data, cl.sessions[sid].mid);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 24:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.OnHardRSA(cl.sessions[sid].mid, cl.sessions[sid].tempFile, keyxml, keysize);//InMemoryRSA(cl.sessions[sid].mid, data, keyxml, keysize);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 28:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.OnHardRSADecrypt(cl.sessions[sid].mid, cl.sessions[sid].tempFile, keyxml, keysize);
                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    case 32:
                                        {
                                            DataSet ds = NewSQLHelper.ExecuteDataSet(NewSQLHelper.ConnString, CommandType.StoredProcedure, "src_getKey", 0, new SqlParameter { ParameterName = "@id", SqlDbType = SqlDbType.Int, Value = Utils.ConvertBytesToInt(cl.sessions[sid].cryptoarg) });
                                            int keysize = int.Parse(ds.Tables[0].Rows[0][""].ToString());
                                            string keyxml = ds.Tables[0].Rows[0][""].ToString();
                                            response = RSAManager.OnHardSign(cl.sessions[sid].tempFile, keyxml, cl.sessions[sid].mid);

                                            cl.sessions[sid].messagePump.Clear();
                                            cl.sessions[sid].messagePump.AddRange(response);
                                            break;
                                        }
                                    default:
                                        return new Rezult(CK_ERRORS.CK_NOT_SUPPORTED);
                                }


                                if (response == null)
                                    return new Rezult { error = CK_ERRORS.CK_NOT_SUPPORTED };
                                else
                                    return new Rezult { data = Utils.ConvertIntToByteArray(response.Length) };
                            }
                            else
                                return new Rezult { error = CK_ERRORS.CK_BAD_SESSION };
                        }
                        else
                            return new Rezult { error = CK_ERRORS.CK_UNRECOGNIZED_CHANEL };
                    }

                default:
                    return new Rezult(CK_ERRORS.CK_NOT_SUPPORTED);
            }
            //return new Rezult();
        }
    }
    public class Rezult
    {
        public int error = 0;
        public byte[] data = null;
        public bool AreErrors
        {
            get
            {
                if (error <= 0)
                    return false;
                else
                    return true;
            }
        }
        public Rezult(byte val)
        {
            data = new byte[1];
            data[0] = val;
            error = 0;
        }
        public Rezult()
        {

        }
        public Rezult(int error)
        {
            this.error = error;
        }
        public static Rezult ReturnError(int error)
        {
            Rezult r = new Rezult();
            r.error = error;
            return r;
        }
    }



}
