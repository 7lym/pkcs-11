﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    public static class Global
    {
        public static ClientManager manager = null;//manager pentru clienti
        private static HashSet<int> _baseFunc = new HashSet<int>();//hashset de id-rui de functii -- dorim acces rapid

        /// <summary>Obtinere hastable de id-uri de functii base.
        /// <para>.</para>
        /// </summary>
        public static HashSet<int> BaseFunctions
        {
            get
            {
                if (_baseFunc.Count == 0)
                {
                    for (int i = 1; i <= 15; i++)
                        _baseFunc.Add(i);
                }
                return _baseFunc;
            }
        }
        public static readonly int limit = 36;
    }
}
