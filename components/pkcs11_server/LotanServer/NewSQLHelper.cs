﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    public class NewSQLHelper
    {
        public static int GetTimeout
        {
            get
            {
                return _timeout;
            }
        }
        public static int SetTimeout
        {
            set
            {
                _timeout = value;
            }
        }
        static private int _timeout = 120;
        private static string _ConnString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["connStr"].ConnectionString;
            }
        }
        private static string _connStr = null;
        public static string ConnString
        {
            get
            {
                if (_connStr != null)
                    return _connStr;
                else
                {
                    _connStr = _ConnString;
                    return _connStr;
                }
            }
        }
        private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());

        public static string GetSQL(string procname, SqlParameter[] parameters)
        {
            string res = "";
            if (parameters != null)
            {
                int i = 0;
                if (parameters.Length > 0)
                    foreach (SqlParameter s in parameters)
                    {
                        if (i > 0)
                            res += ", ";

                        if (s != null)
                            res += ("'" + s.Value.ToString() + "'");

                        i++;
                    }
            }

            return procname + " " + res;
        }
        static public DataSet ExecuteDataSet(string ConnexionString, CommandType type, string Command, int TimeOut = 0, params SqlParameter[] parameters)
        {
            try
            {
                //WriteDebugParams(Command, parameters);
                DataSet ds = new DataSet();
                bool throwException = false;
                if (string.IsNullOrEmpty(ConnexionString) || ConnexionString.Length < 5)
                    throw new ArgumentNullException("connectionString");
                if (string.IsNullOrEmpty(Command) || string.IsNullOrWhiteSpace(Command))
                    throw new ArgumentNullException("commandText");
                using (SqlCommand command = new SqlCommand())
                {
                    if (command != null)
                    {
                        if (TimeOut > 0)
                            command.CommandTimeout = TimeOut;
                        else
                            command.CommandTimeout = _timeout;
                        command.CommandText = Command;
                        command.CommandType = type;
                        if (parameters != null)
                        {
                            foreach (SqlParameter p in parameters)
                            {
                                if (p != null)
                                {
                                    // Check for derived output value with no value assigned
                                    if ((p.Direction == ParameterDirection.InputOutput ||
                                        p.Direction == ParameterDirection.Input) &&
                                        (p.Value == null))
                                    {
                                        p.Value = DBNull.Value;
                                    }
                                    command.Parameters.Add(p);
                                }
                            }
                        }
                        using (SqlConnection connexion = new SqlConnection(ConnexionString))
                        {
                            if (connexion != null)
                            {
                                connexion.Open();
                                command.Connection = connexion;
                                using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                                {
                                    dataAdapter.Fill(ds);
                                }
                                connexion.Close();
                            }
                            else
                                throwException = true;
                        }
                    }
                    else
                        throwException = true;
                }
                if (throwException)
                    throw new Exception("The command could't be created or executed");
                return ds;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static SqlParameter[] GetSpParameterSet(string connectionString, string spName, bool includeReturnValueParameter)
        {
            if (connectionString == null || connectionString.Length == 0) throw new ArgumentNullException("connectionString");
            if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");
            string hashKey = connectionString + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");
            SqlParameter[] cachedParameters;

            cachedParameters = paramCache[hashKey] as SqlParameter[];
            if (cachedParameters == null)
            {
                using (SqlCommand cmd = new SqlCommand(spName))
                {
                    cmd.CommandTimeout = _timeout;
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        cmd.Connection = connection;
                        SqlCommandBuilder.DeriveParameters(cmd);
                        connection.Close();
                        if (!includeReturnValueParameter)
                        {
                            cmd.Parameters.RemoveAt(0);
                        }

                        SqlParameter[] discoveredParameters = new SqlParameter[cmd.Parameters.Count];

                        cmd.Parameters.CopyTo(discoveredParameters, 0);

                        // Init the parameters with a DBNull value
                        foreach (SqlParameter discoveredParameter in discoveredParameters)
                        {
                            discoveredParameter.Value = DBNull.Value;
                        }
                        paramCache[hashKey] = discoveredParameters;
                        return CloneParameters(discoveredParameters);
                    }
                }
            }
            else
                return CloneParameters(cachedParameters);
        }
        private static SqlParameter[] CloneParameters(SqlParameter[] originalParameters)
        {
            SqlParameter[] clonedParameters = new SqlParameter[originalParameters.Length];

            for (int i = 0, j = originalParameters.Length; i < j; i++)
            {
                clonedParameters[i] = (SqlParameter)((ICloneable)originalParameters[i]).Clone();
            }

            return clonedParameters;
        }
        private static void AssignParameterValues(SqlParameter[] commandParameters, object[] parameterValues)
        {
            if ((commandParameters == null) || (parameterValues == null))
            {
                // Do nothing if we get no data
                return;
            }

            // We must have the same number of values as we pave parameters to put them in
            if (commandParameters.Length != parameterValues.Length)
            {
                throw new ArgumentException("Parameter count does not match Parameter Value count.");
            }

            // Iterate through the SqlParameters, assigning the values from the corresponding position in the
            // value array
            for (int i = 0, j = commandParameters.Length; i < j; i++)
            {
                // If the current array value derives from IDbDataParameter, then assign its Value property
                if (parameterValues[i] is IDbDataParameter)
                {
                    IDbDataParameter paramInstance = (IDbDataParameter)parameterValues[i];
                    if (paramInstance.Value == null)
                    {
                        commandParameters[i].Value = DBNull.Value;
                    }
                    else
                    {
                        commandParameters[i].Value = paramInstance.Value;
                    }
                }
                else if (parameterValues[i] == null)
                {
                    commandParameters[i].Value = DBNull.Value;
                }
                else
                {
                    commandParameters[i].Value = parameterValues[i];
                }
            }
        }

        private static string GenericCreateErrorMessage(string name, object[] parameters)
        {
            StringBuilder bld = new StringBuilder();
            bld.Append("Procedura : ");
            bld.Append(name);
            bld.Append(" Parametrii ");
            foreach (object param in parameters)
            {
                if (param != null)
                {
                    bld.Append(param.ToString());
                    bld.Append(" ");
                }
            }
            return bld.ToString();
        }




        public static int ExecuteNonQuery(string ConnexionString, CommandType type, string Command, int TimeOut = 0, params SqlParameter[] parameters)
        {
            try
            {

                int rez = -1;
                bool throwException = false;
                if (string.IsNullOrEmpty(ConnexionString) || ConnexionString.Length < 5)
                    throw new ArgumentNullException("connectionString");
                if (string.IsNullOrEmpty(Command) || string.IsNullOrWhiteSpace(Command))
                    throw new ArgumentNullException("commandText");
                using (SqlCommand command = new SqlCommand())
                {
                    if (command != null)
                    {
                        if (TimeOut > 0)
                            command.CommandTimeout = TimeOut;
                        else
                            command.CommandTimeout = _timeout;
                        command.CommandText = Command;
                        command.CommandType = type;
                        if (parameters != null)
                        {
                            foreach (SqlParameter p in parameters)
                            {
                                if (p != null)
                                {
                                    // Check for derived output value with no value assigned
                                    if ((p.Direction == ParameterDirection.InputOutput ||
                                        p.Direction == ParameterDirection.Input) &&
                                        (p.Value == null))
                                    {
                                        p.Value = DBNull.Value;
                                    }
                                    command.Parameters.Add(p);
                                }
                            }
                        }
                        using (SqlConnection connexion = new SqlConnection(ConnexionString))
                        {
                            if (connexion != null)
                            {
                                connexion.Open();
                                command.Connection = connexion;
                                rez = command.ExecuteNonQuery();
                                connexion.Close();
                            }
                            else
                                throwException = true;
                        }
                    }
                    else
                        throwException = true;
                }
                if (throwException)
                    throw new Exception("The command could't be created or executed");
                return rez;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }




        static public object ExecuteScalar(string ConnexionString, CommandType type, string Command, int TimeOut = 0, params SqlParameter[] parameters)
        {
            try
            {

                object rez = null;
                bool throwException = false;
                if (string.IsNullOrEmpty(ConnexionString) || ConnexionString.Length < 5)
                    throw new ArgumentNullException("connectionString");
                if (string.IsNullOrEmpty(Command) || string.IsNullOrWhiteSpace(Command))
                    throw new ArgumentNullException("commandText");
                using (SqlCommand command = new SqlCommand())
                {
                    if (command != null)
                    {
                        if (TimeOut > 0)
                            command.CommandTimeout = TimeOut;
                        else
                            command.CommandTimeout = _timeout;
                        command.CommandText = Command;
                        command.CommandType = type;
                        if (parameters != null)
                        {
                            foreach (SqlParameter p in parameters)
                            {
                                if (p != null)
                                {
                                    // Check for derived output value with no value assigned
                                    if ((p.Direction == ParameterDirection.InputOutput ||
                                        p.Direction == ParameterDirection.Input) &&
                                        (p.Value == null))
                                    {
                                        p.Value = DBNull.Value;
                                    }
                                    command.Parameters.Add(p);
                                }
                            }
                        }
                        using (SqlConnection connexion = new SqlConnection(ConnexionString))
                        {
                            if (connexion != null)
                            {
                                connexion.Open();
                                command.Connection = connexion;
                                //using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                                //{
                                //    dataAdapter.Fill(ds);
                                //}
                                rez = command.ExecuteScalar();
                                connexion.Close();
                            }
                            else
                                throwException = true;
                        }
                    }
                    else
                        throwException = true;
                }
                if (throwException)
                    throw new Exception("The command could't be created or executed");
                return rez;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        static public object ExecuteScalarVector(string ConnexionString, CommandType type, string Command, SqlParameter[] parameters, int TimeOut = 0)
        {
            try
            {

                object rez = null;
                bool throwException = false;
                if (string.IsNullOrEmpty(ConnexionString) || ConnexionString.Length < 5)
                    throw new ArgumentNullException("connectionString");
                if (string.IsNullOrEmpty(Command) || string.IsNullOrWhiteSpace(Command))
                    throw new ArgumentNullException("commandText");
                using (SqlCommand command = new SqlCommand())
                {
                    if (command != null)
                    {
                        if (TimeOut > 0)
                            command.CommandTimeout = TimeOut;
                        else
                            command.CommandTimeout = _timeout;
                        command.CommandText = Command;
                        command.CommandType = type;
                        if (parameters != null)
                        {
                            foreach (SqlParameter p in parameters)
                            {
                                if (p != null)
                                {
                                    // Check for derived output value with no value assigned
                                    if ((p.Direction == ParameterDirection.InputOutput ||
                                        p.Direction == ParameterDirection.Input) &&
                                        (p.Value == null))
                                    {
                                        p.Value = DBNull.Value;
                                    }
                                    command.Parameters.Add(p);
                                }
                            }
                        }
                        using (SqlConnection connexion = new SqlConnection(ConnexionString))
                        {
                            if (connexion != null)
                            {
                                connexion.Open();
                                command.Connection = connexion;
                                //using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                                //{
                                //    dataAdapter.Fill(ds);
                                //}
                                rez = command.ExecuteScalar();
                                connexion.Close();
                            }
                            else
                                throwException = true;
                        }
                    }
                    else
                        throwException = true;
                }
                if (throwException)
                    throw new Exception("The command could't be created or executed");
                return rez;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static object ExecuteScalarWithoutParams(string connectionString, string spName, params object[] parameterValues)
        {
            try
            {
                if (string.IsNullOrEmpty(connectionString) || connectionString.Length < 5) throw new ArgumentNullException("connection");
                if (spName == null || spName.Length == 0) throw new ArgumentNullException("spName");

                // If we receive parameter values, we need to figure out where they go
                if ((parameterValues != null) && (parameterValues.Length > 0))
                {
                    // Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                    SqlParameter[] commandParameters = GetSpParameterSet(connectionString, spName, false);

                    // Assign the provided values to these parameters based on parameter order
                    AssignParameterValues(commandParameters, parameterValues);

                    // Call the overload that takes an array of SqlParameters
                    return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, _timeout, commandParameters);
                }
                else
                {
                    // Otherwise we can just call the SP without params
                    return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, _timeout);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
