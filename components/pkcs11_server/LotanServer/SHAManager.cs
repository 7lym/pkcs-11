﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    public class SHAManager
    {
        
        public static byte[] OnHardHash(string file,int shaType)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                switch (shaType)
                {
                    case CK_MECANISM.CK_SHA.SHA256:
                        return ComputeSha256(stream);
                    case CK_MECANISM.CK_SHA.SHA384:
                        return ComputeSha384(stream);
                    case CK_MECANISM.CK_SHA.SHA512:
                        return ComputeSha512(stream);
                    case CK_MECANISM.CK_SHA.SHA224:
                    default:
                        return null;
                }
            }
        }

        public static byte[] InMemoryHash(byte[] data,int shaType)
        {
            using (MemoryStream stream = new MemoryStream(data))
            {
                switch (shaType)
                {
                    case CK_MECANISM.CK_SHA.SHA256:
                        return ComputeSha256(stream);
                    case CK_MECANISM.CK_SHA.SHA384:
                        return ComputeSha384(stream);
                    case CK_MECANISM.CK_SHA.SHA512:
                        return ComputeSha512(stream);
                    case CK_MECANISM.CK_SHA.SHA224:
                    default:
                        return null;
                }

                //return ComputeSha256(stream);
            }
        }

        private static byte[] ComputeSha256(Stream stream)
        {
            SHA256 mySHA256 = SHA256Managed.Create();
            return mySHA256.ComputeHash(stream);
        }
        private static byte[] ComputeSha384(Stream stream)
        {
            SHA384 mySHA384 = SHA384Managed.Create();
            return mySHA384.ComputeHash(stream);
        }
        private static byte[] ComputeSha512(Stream stream)
        {
            SHA512 mySHA512 = SHA512Managed.Create();
            return mySHA512.ComputeHash(stream);
        }
    }
}
