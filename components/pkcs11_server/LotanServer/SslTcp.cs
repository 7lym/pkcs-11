﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LotanServer
{
    public sealed class SslTcpServer
    {
        const int DATA_SIZE = 4096;
        
        
        static X509Certificate serverCertificate = null;
        // The certificate parameter specifies the name of the file 
        // containing the machine certificate.
        public static void RunServer(string certificate)
        {
            serverCertificate = X509Certificate.CreateFromCertFile(certificate);
            
            // Create a TCP/IP (IPv4) socket and listen for incoming connections.
            TcpListener listener = new TcpListener(IPAddress.Any, 18081);
            listener.Start();
            while (true)
            {
                Console.WriteLine("Waiting for a client to connect...");
                // Application blocks while waiting for an incoming connection.
                // Type CNTL-C to terminate the server.
                TcpClient client = listener.AcceptTcpClient();
                Thread thr = new Thread(new ParameterizedThreadStart(ProcessClient));
                thr.Start(client);
                //ProcessClient(client);
            }
        }
        static void ProcessClient(object obj)
        {
            TcpClient client = (TcpClient)obj;
            // A client has connected. Create the 
            // SslStream using the client's network stream.
            SslStream sslStream = new SslStream(
                client.GetStream(), false);
            // Authenticate the server but don't require the client to authenticate.
            try
            {
                sslStream.AuthenticateAsServer(serverCertificate,
                    false, SslProtocols.Tls, true);
                // Display the properties and settings for the authenticated stream.
                //DisplaySecurityLevel(sslStream);
                //DisplaySecurityServices(sslStream);
                //DisplayCertificateInformation(sslStream);
                //DisplayStreamProperties(sslStream);

                // Set timeouts for the read and write to 5 seconds.
                sslStream.ReadTimeout = 500000;
                sslStream.WriteTimeout = 500000;
 
                Console.WriteLine("Waiting for client message...");

                try
                {
                    byte[] data = ReadMessage(sslStream);
                    if (data.Length < 8 || (data.Length > 8 && data.Length < 21))
                        SendMessage(ParseRezult(new Rezult {error=CK_ERRORS.CK_UNRECOGNIZED_PACKET },""),sslStream);
                    else
                    {
                        if (data.Length == 8)
                        {
                            byte[] sid=CIDGenerator.CreateHash(data);
                            string key = Utils.ConvertByteToString(sid);
                            Global.manager.addClient(key);
                            Rezult rez = new Rezult { error=0 };
                            SendMessage(ParseRezult(rez, key), sslStream);

                        }
                        else
                        {
                            string key = Utils.ConvertByteToString(data.SubArray(0, 20));
                            Rezult rez = Functions.RootFunction(sslStream, data.SubArray(20, data.Length - 20), key);
                            SendMessage(ParseRezult(rez, key,false), sslStream);
                        }
                    }
                   
                }
                catch (Exception ex)
                {
                    SendMessage(ParseRezult(new Rezult { error = CK_ERRORS.CK_UNABLE_TO_PROCESS_PACKET }, ""), sslStream);
                }
                // Write a message to the client.
                
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                sslStream.Close();
                client.Close();
                return;
            }
            finally
            {
                // The client stream will be closed with the sslStream
                // because we specified this behavior when creating
                // the sslStream.
                sslStream.Close();
                client.Close();
            }
        }

        public static bool ExceedTime(DateTime started)
        {
            if ((DateTime.Now - started).TotalSeconds > 45)
                return true;
            return false;
        }

        static byte[] ReadMessage(SslStream sslStream)
        {
            DateTime cooler = DateTime.Now;
            byte[] lenght = new byte[4];
            List<byte> len = new List<byte>();
            int read = 0;
            while (read<4)
            {
                int r = 0;
                r= sslStream.Read(lenght, 0, 4);
                len.AddRange(lenght.SubArray(0, r));
                read += r;
            }
            sslStream.WriteByte(1);
            int packetLenght = Utils.ConvertBytesToInt(len.ToArray());
            List<byte> packet = new List<byte>();
            byte[] data = new byte[DATA_SIZE];
            read = 0;
            while (read < packetLenght)
            {
                int r = 0;
                r = sslStream.Read(data, 0, 4096);
                packet.AddRange(data.SubArray(0, r));
                read += r;
            }
            sslStream.WriteByte(1);
            return packet.ToArray();
        }
        static public  byte[] ParseRezult(Rezult rezult, string chanelKey,bool addChanelID=true)
        {
            List<byte> list = new List<byte>();
            if (!rezult.AreErrors)
                list.AddRange(Utils.ConvertIntToByteArray(CK_ERRORS.CK_TRUE));
            else
                list.AddRange(Utils.ConvertIntToByteArray(rezult.error));
            if (chanelKey.Length>1&&addChanelID)
                list.AddRange(Utils.ConvertStringToBytes(chanelKey));
            //if (rezult.AreErrors)
            //    list.AddRange(Utils.ConvertIntToByteArray(rezult.error));
            if (rezult.data != null && rezult.data.Length > 0)
            {
                //list.AddRange(Utils.ConvertIntToByteArray(rezult.data.Length));
                list.AddRange(rezult.data);
            }


            return list.ToArray();
        }
        static void SendMessage (byte[] data,SslStream sslSteam)
        {
            sslSteam.Write(Utils.ConvertIntToByteArray(data.Length), 0, 4);
            string err = Utils.ConvertBytesToInt(data.SubArray(0, 4)) == 0 ? "no erors" : "erors";
            Console.WriteLine("executed with " +err);
            byte[] responses = new byte[10];
            sslSteam.Read(responses, 0, 1);
            if (responses[0]==1)
            {
                sslSteam.Write(data, 0, data.Length);
                sslSteam.Read(responses, 0, 1);
            }
        }
        static void DisplaySecurityLevel(SslStream stream)
        {
            Console.WriteLine("Cipher: {0} strength {1}", stream.CipherAlgorithm, stream.CipherStrength);
            Console.WriteLine("Hash: {0} strength {1}", stream.HashAlgorithm, stream.HashStrength);
            Console.WriteLine("Key exchange: {0} strength {1}", stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
            Console.WriteLine("Protocol: {0}", stream.SslProtocol);
        }
        static void DisplaySecurityServices(SslStream stream)
        {
            Console.WriteLine("Is authenticated: {0} as server? {1}", stream.IsAuthenticated, stream.IsServer);
            Console.WriteLine("IsSigned: {0}", stream.IsSigned);
            Console.WriteLine("Is Encrypted: {0}", stream.IsEncrypted);
        }
        static void DisplayStreamProperties(SslStream stream)
        {
            Console.WriteLine("Can read: {0}, write {1}", stream.CanRead, stream.CanWrite);
            Console.WriteLine("Can timeout: {0}", stream.CanTimeout);
        }
        static void DisplayCertificateInformation(SslStream stream)
        {
            Console.WriteLine("Certificate revocation list checked: {0}", stream.CheckCertRevocationStatus);

            X509Certificate localCertificate = stream.LocalCertificate;
            if (stream.LocalCertificate != null)
            {
                Console.WriteLine("Local cert was issued to {0} and is valid from {1} until {2}.",
                    localCertificate.Subject,
                    localCertificate.GetEffectiveDateString(),
                    localCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Local certificate is null.");
            }
            // Display the properties of the client's certificate.
            X509Certificate remoteCertificate = stream.RemoteCertificate;
            if (stream.RemoteCertificate != null)
            {
                Console.WriteLine("Remote cert was issued to {0} and is valid from {1} until {2}.",
                    remoteCertificate.Subject,
                    remoteCertificate.GetEffectiveDateString(),
                    remoteCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Remote certificate is null.");
            }
        }
        private static void DisplayUsage()
        {
            Console.WriteLine("To start the server specify:");
            Console.WriteLine("serverSync certificateFile.cer");
            Environment.Exit(1);
        }
        public static int Start(string args)
        {
            string certificate = null;
            if (args == null || args.Length < 1)
            {
                DisplayUsage();
            }
            certificate = args;
            SslTcpServer.RunServer(certificate);
            return 0;
        }
    }
   
}
