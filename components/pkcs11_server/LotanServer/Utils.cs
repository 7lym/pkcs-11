﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace LotanServer
{
    public static class Utils
    {
        public static byte[] ConvertIntToByteArray(int intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            //if (BitConverter.IsLittleEndian)
            //    Array.Reverse(intBytes);
            //byte[] result = intBytes;
            
            return intBytes;
        }
        public static byte[] ConvertIntToByteArray(int intValue,bool tolong)
        {
            Int64 val = intValue;
            return ConvertIntToByteArray(val);
        }
        public static byte[] ConvertIntToByteArray(Int64 intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            //byte[] result = intBytes;
            return intBytes;
        }
        public static Int32 ConvertBytesToInt(byte[]intValue)
        {
            //Array.Reverse(intValue);
            return BitConverter.ToInt32(intValue, 0);
        }
        public static Int64 ConvertBytesToLong(byte[] data)
        {
            return BitConverter.ToInt64(data, 0);
        }
        public static byte[] ConvertStringToBytes(string strValue)
        {
            List<byte> list = new List<byte>();
            foreach (char ch in strValue)
                list.Add(Convert.ToByte(ch));
            byte[] aux = list.ToArray();
            //Array.Reverse(aux);
            return aux;
        }
        public static string ConvertByteToString(byte[] array)
        {
            // return System.Text.Encoding.Default.GetString(bytes);
            List<char> list = new List<char>();
            foreach (byte oct in array)
                list.Add(Convert.ToChar(oct));
            StringBuilder bld = new StringBuilder();
            foreach (char ch in list)
                bld.Append(ch.ToString());
            return bld.ToString();
        }
        public static List<byte> BytePadding(int pad)
        {
            List<byte> list = new List<byte>();
            byte[]data= ConvertStringToBytes(" ");
            for (int i = 0; i < pad; i++)
                list.Add(data[0]);
            return list;

        }
        public static byte[] ConvertListIntegerToBytes (List<int> integers)
        {
            List<byte> list = new List<byte>();
            foreach (int integer in integers)
                list.AddRange(ConvertIntToByteArray(integer));
            return list.ToArray();
        }
        public static byte[] AddLenghtByteChunk(byte[] bytes)
        {
            List<byte> chunk = new List<byte>();
            chunk.AddRange(ConvertIntToByteArray(bytes.Length));
            chunk.AddRange(bytes);
            return chunk.ToArray();
            
        }

        public static void AppendAllBytes(string path, byte[] bytes)
        {
            

            using (var stream = new FileStream(path, FileMode.Append))
            {
                stream.Write(bytes, 0, bytes.Length);
            }
        }

    }
    

    public static class CIDGenerator
    {
        static HashSet<int> seeds = new HashSet<int>();
        static int GetRandomUnique()
        {
            Random rand = new Random();
            int value = rand.Next(int.MinValue, int.MaxValue);
            while(seeds.Contains(value))
            {
                value = rand.Next(int.MinValue, int.MinValue);
            }
            seeds.Add(value);
            return value;
        }
        static int GetRandom()
        {
            Random rand = new Random();
            return rand.Next(int.MinValue, int.MinValue);
        }
        static public byte [] CreateHash(byte[] data)
        {
            //byte[] data = null;
            List<byte> bytes = new List<byte>();
            bytes.AddRange(Utils.ConvertIntToByteArray(GetRandomUnique()));
            bytes.AddRange(Utils.ConvertIntToByteArray(GetRandom()));
            bytes.AddRange(Utils.ConvertIntToByteArray(GetRandom()));
            bytes.AddRange(data);
            SHA1 sha = new SHA1CryptoServiceProvider();
            string partial = Convert.ToBase64String(sha.ComputeHash(bytes.ToArray()));
            return Utils.ConvertStringToBytes(partial.Substring(0, 20));
            //return data;
        }
    }

}
