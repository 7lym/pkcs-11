USE [master]
GO
/****** Object:  Database [smartserver]    Script Date: 1/28/2018 7:47:07 PM ******/
CREATE DATABASE [smartserver]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'smartserver', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smartserver.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'smartserver_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smartserver_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [smartserver] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [smartserver].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [smartserver] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [smartserver] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [smartserver] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [smartserver] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [smartserver] SET ARITHABORT OFF 
GO
ALTER DATABASE [smartserver] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [smartserver] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [smartserver] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [smartserver] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [smartserver] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [smartserver] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [smartserver] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [smartserver] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [smartserver] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [smartserver] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [smartserver] SET  DISABLE_BROKER 
GO
ALTER DATABASE [smartserver] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [smartserver] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [smartserver] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [smartserver] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [smartserver] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [smartserver] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [smartserver] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [smartserver] SET RECOVERY FULL 
GO
ALTER DATABASE [smartserver] SET  MULTI_USER 
GO
ALTER DATABASE [smartserver] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [smartserver] SET DB_CHAINING OFF 
GO
ALTER DATABASE [smartserver] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [smartserver] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'smartserver', N'ON'
GO
USE [smartserver]
GO
/****** Object:  StoredProcedure [dbo].[src_Authentificate]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[src_Authentificate]
	-- Add the parameters for the stored procedure here
	(
	@pin nvarchar(100)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select * from PIN as P where P.PIN =@pin
    -- Insert statements for procedure here
	
END

GO
/****** Object:  StoredProcedure [dbo].[src_GetAtribute]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[src_GetAtribute]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select * from AtributeTable
    -- Insert statements for procedure here
	
END

GO
/****** Object:  StoredProcedure [dbo].[src_GetAtributeValues]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[src_GetAtributeValues]
	-- Add the parameters for the stored procedure here
	(
	@id int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select * from AtributeTable where ID=@id
    -- Insert statements for procedure here
	
END

GO
/****** Object:  StoredProcedure [dbo].[src_getKey]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[src_getKey]
	-- Add the parameters for the stored procedure here
	(
	@id int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select top 1 KeySize,XmlKey from CryptoKeys where ID=@id
    -- Insert statements for procedure here
	
END

GO
/****** Object:  Table [dbo].[AtributeTable]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AtributeTable](
	[ID] [int] NULL,
	[CKA_CLASS] [nvarchar](50) NULL,
	[CKA_TOKEN] [nvarchar](50) NULL,
	[CKA_PRIVATE] [nvarchar](50) NULL,
	[CKA_OBJECT_ID] [nvarchar](4000) NULL,
	[CKA_CERTIFICATE_TYPE] [nvarchar](4000) NULL,
	[CKA_KEY_TYPE] [nvarchar](50) NULL,
	[CKA_SUBJECT] [nvarchar](50) NULL,
	[CKA_ENCRYPT] [nvarchar](50) NULL,
	[CKA_DECRYPT] [nvarchar](50) NULL,
	[CKA_SIGN] [nvarchar](50) NULL,
	[CKA_VALUE] [nvarchar](4000) NULL,
	[JustForLogged] [int] NOT NULL CONSTRAINT [DF__AtributeT__JustF__21B6055D]  DEFAULT ((0))
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CryptoKeys]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CryptoKeys](
	[ID] [int] NOT NULL,
	[XmlKey] [ntext] NOT NULL,
	[KeySize] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PIN]    Script Date: 1/28/2018 7:47:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PIN](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PIN] [nvarchar](100) NOT NULL
) ON [PRIMARY]

GO
INSERT [dbo].[AtributeTable] ([ID], [CKA_CLASS], [CKA_TOKEN], [CKA_PRIVATE], [CKA_OBJECT_ID], [CKA_CERTIFICATE_TYPE], [CKA_KEY_TYPE], [CKA_SUBJECT], [CKA_ENCRYPT], [CKA_DECRYPT], [CKA_SIGN], [CKA_VALUE], [JustForLogged]) VALUES (1, N'AQAAAA==', N'AAAAAA==', N'AAAAAA==', N'hIaGWNFN0IEbLdwsFS3XyL3kDP4=', N'AAAAAA==', N'AAAAAA==', N'', N'AAAAAA==', N'AAAAAA==', N'AAAAAA==', N'MIIDYTCCAkmgAwIBAgIQBTpf3uod+iwrt2T5A/gFmjANBgkqhkiG9w0BAQUFADA2MSEwHwYJKoZIhvcNAQkBFhJyb290N2x5bUB5YWhvby5jb20xETAPBgNVBAMMCHJvb3Q3bHltMB4XDTE4MDEyODE0NTU0NVoXDTE5MDEyODE0NTU0NVowJDESMBAGCSqGSIb3DQEJARYDbWVAMQ4wDAYDVQQDDAVsb3RhbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJASj1MlhzcKrdr3uAB9jSZ/NRGt3b67YOXQO7+hZOc4Yt6QR5NUjyrrT+s2YD5DNdVmpEcwtOLrjYplRcbC8pghcDDO20vxP4GKtR54IRfGD6aV77uTPXx5ucrS8qD/4jJjGl8IAIl+MxAgCNfe7sq0NSxia7HK1hw+DGL1AQMDce6khRU7jEKx8AjRpREATJIJQgslYVhNRxoUYtQDwBeBeGklGpcuMohTdM5RO/p1M1yWiaPwg1pcjx+thepupUe/rVqAwbsDQyPDtwo53+bBmyOxAyaHETvgB//IqMeG8xhmoy0XpPaY5H8Z+e6uEwj0J3n3d4M9CW/BQeTuw0sCAwEAAaN9MHswCwYDVR0PBAQDAgTwMCwGA1UdJQEB/wQiMCAGCCsGAQUFBwMEBggrBgEFBQcDAgYKKwYBBAGCNwoDDDAdBgNVHQ4EFgQUj3Zk+OJ2GPWiuQf2grnIz5r0vMMwHwYDVR0jBBgwFoAUvxEaW9a0OU7+tLjH2Em2ZnltbIMwDQYJKoZIhvcNAQEFBQADggEBAA1pjOz2lDJ3qJIWhpoW48GYLNY0UB7X2g2twEXNSeLXpDOtAYSFb62FczlfYpnv4ZZdRlBltoGx2k1iLS67lp5//gy06qG6BuuawIsSoKwCqQ9qQwY1ok2n3di623E54sK/JmpF1mG8SNg5flvtyYSYcEjp3W5/71PWGHZKZ8YACr4UFoY4J3WVCWHCYIBAdDTZO7XUpdBDKyGjiLfVlKr3ZNCDELaU7p584aiJeS5UK/pbiWnC6kXWNiFsAeMSrEc7W+Fj1hZsPNJv3VeRkQNzuj5KHxChthEx4p/ckTcNZRs443FzzNajrwPNQpyazoA9iIe+eBdMJEIdxUs0Dwo=', 0)
INSERT [dbo].[AtributeTable] ([ID], [CKA_CLASS], [CKA_TOKEN], [CKA_PRIVATE], [CKA_OBJECT_ID], [CKA_CERTIFICATE_TYPE], [CKA_KEY_TYPE], [CKA_SUBJECT], [CKA_ENCRYPT], [CKA_DECRYPT], [CKA_SIGN], [CKA_VALUE], [JustForLogged]) VALUES (2, N'AwAAAA==', N'AAAAAA==', N'AAAAAA==', N'hIaGWNFN0IEbLdwsFS3XyL3kDP4=', N'AAAAAA==', N'AAAAAA==', N'', N'AAAAAA==', N'AAAAAA==', N'AAAAAA==', N'AAAAAA==', 1)
INSERT [dbo].[CryptoKeys] ([ID], [XmlKey], [KeySize]) VALUES (2, N'<RSAKeyValue><Modulus>kBKPUyWHNwqt2ve4AH2NJn81Ea3dvrtg5dA7v6Fk5zhi3pBHk1SPKutP6zZgPkM11WakRzC04uuNimVFxsLymCFwMM7bS/E/gYq1HnghF8YPppXvu5M9fHm5ytLyoP/iMmMaXwgAiX4zECAI197uyrQ1LGJrscrWHD4MYvUBAwNx7qSFFTuMQrHwCNGlEQBMkglCCyVhWE1HGhRi1APAF4F4aSUaly4yiFN0zlE7+nUzXJaJo/CDWlyPH62F6m6lR7+tWoDBuwNDI8O3Cjnf5sGbI7EDJocRO+AH/8iox4bzGGajLRek9pjkfxn57q4TCPQnefd3gz0Jb8FB5O7DSw==</Modulus><Exponent>AQAB</Exponent><P>xtIcXRtGVxZ3f8kRNhUm9W3LQLghl5n2w60afbs97K8pFfsWK1dkI118Ha5I3dn0Ft3BR9R3tDMQgS80qYIn8dbMV74/uJmN7FERgLnyElmWsE8SfSiM1E9p791bAgea4ZRKEVxaxkxK9adSpNwHDgRE1m/J6dfe3UtrZVX4qV0=</P><Q>uYGwVmvr/YfCZIWPNGe1uXAQ88E+u5UanKwuMu5U6Te8QH63EzO8OE7+TebF7y207a04azvyDnOsQdQcq7O0zz+1Q9721j4qzB3rU0suvNYBe6s4b0fjD49flQKnVpng1JrafznHn6pXk6eF37MRTzZyDvaOWX5beHS/viyRzMc=</Q><DP>Uk4Eg96YndClI3ce8t6KxCrxaxdOgK2x44szXj03A1X3kivhH4wL3kAC816Et6JUU2r1akDgVO6WtIPQLyO8WXbsHJ6bTlXy1JMyxbE0Lrh7kv0I0FsLAZDmb++YjxyK1ztF9i3fPhuzJEJiCfEz0m+d949LbMx6a+2exdJzH9k=</DP><DQ>ZruBsnqV+JPkfdLRWc5Nf2KmoLdWTH/oeveHpkEC2R06reT+8i1mgTCN+Qpid3CykCWr5wcNqB87OEGFlzkwWs4HToz92ewXu26eUkSCn6VMsPZDjK0c/BMEdftAPN9++X7AnlCbUrr9QRI3O4lpfprCU7MLZlhpw620OldobyU=</DQ><InverseQ>IssqHoz/YjUQFa0RKRNP/9Ni7tbCNfo4127iKoTn3L5EPuMlZdOeMkQiYEl5YFebC+bG81/m9K1eKBJRVctTFlWcx6gFIXLAU/8yk7KdUnDAGyWOF1QPtBLmX3lix73aWV7EMtJGlAcJFrJUYNg+LRunzWC9nWh/HAQ39Y4A0O0=</InverseQ><D>UcoHjadPZMLuQdHBpp2gBQvQP9IZgY70imK1mKtyEgWjsz/rMkud3zqpKxm6d35GUHitqDCRcAVTXA70FstTV2QcQckdG7bAW7U37y05dDqfZe3WpAEJteGQxQEOdrHtYrd6IBr12xhGus/E1XAc1fyJ90GEa/F/rZbYB1GZmnB/DW2Z7G11Jix0U+Zgn/q8K64r9uYMjQIRlL/OzGM/wArTBQI2zK0KndJPdy8sty+NILhN5IBqY07DXBJGICx2hDH9dh1qWDhYGBqUhK9fGuwtzC3FlOgBe8gs9lvp4j8c/DrjPMnLUxozWGjAzlu6U3PZUDRgNXFEOtkZNLhGSQ==</D></RSAKeyValue>', 2048)
INSERT [dbo].[PIN] ([ID], [Name], [PIN]) VALUES (1, N'test', N'test')
USE [master]
GO
ALTER DATABASE [smartserver] SET  READ_WRITE 
GO
