#pragma once
#include "DllServer.h"
#include <iostream>
#include "string"

#include <ws2tcpip.h>
#pragma comment(lib,"ws2_32.lib") //Required for WinSock


#include "PacketType.h"

bool DllServer::sendall(Connection& connection, const char * data, const int totalBytes)
{
	int bytesSent = 0;
	while (bytesSent < totalBytes)
	{
		int retnCheck = send(connection.m_socket, data + bytesSent, totalBytes - bytesSent, 0);
		if (retnCheck < 0)//== SOCKET_ERROR) 
		{
			printf("Error while sending %d\n", WSAGetLastError());
			return false;
		}
		bytesSent += retnCheck;
	}
	return true;
}

bool DllServer::recvall(Connection& connection, char * data, int totalBytes)
{
	int bytesReceived = 0; //Holds the total bytes received
	while (bytesReceived < totalBytes) //While we still have more bytes to recv
	{
		int retnCheck = recv(connection.m_socket, data + bytesReceived, totalBytes - bytesReceived, 0);
		if (retnCheck < 0)
		{
			printf("Error while sendiing %d\n", WSAGetLastError());
			return false;
		}
		std::cout << "Getting bytes " << retnCheck << std::endl;
		if (retnCheck == SOCKET_ERROR || retnCheck == 0)
			return false;
		bytesReceived += retnCheck;
	}
	return true;
}



bool DllServer::getUInt(Connection& connection, unsigned int & int32_t)
{

	if (!recvall(connection, (char*)&int32_t, sizeof(unsigned int)))
		return false;
	//int32_t = ntohl(int32_t); 
	return true;
}

bool DllServer::getUChar(Connection& connection, unsigned char & int8_t)
{
	if (!recvall(connection, (char*)&int8_t, sizeof(char)))
		return false;
	//int8_t = ntohl(int8_t);
	return true;
}



DllServer::~DllServer()
{
	closesocket(m_sListen);
	WSACleanup();
}

DllServer::DllServer(unsigned short int port, bool loopBacktoLocalHost)
{
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);
	if (WSAStartup(DllVersion, &wsaData) != 0)
	{
		MessageBoxA(0, "WinSock startup failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (loopBacktoLocalHost)
		inet_pton(AF_INET, "127.0.0.1", &m_addr.sin_addr.s_addr);
	else
		m_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	m_addr.sin_port = htons(port); //Port
	m_addr.sin_family = AF_INET; //IPv4 Socket

	m_sListen = socket(AF_INET, SOCK_STREAM, 0);
	if (::bind(m_sListen, (SOCKADDR*)&m_addr, sizeof(m_addr)) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to bind the address to our listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (::listen(m_sListen, SOMAXCONN) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to listen on listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}

}

SOCKET DllServer::WaitForDllClient()
{
	int addrlen = sizeof(m_addr);
	SOCKET newConnectionSocket = accept(m_sListen, (SOCKADDR*)&m_addr, &addrlen);
	if (newConnectionSocket == 0 || newConnectionSocket == INVALID_SOCKET)
		return INVALID_SOCKET;
	return newConnectionSocket;
}









