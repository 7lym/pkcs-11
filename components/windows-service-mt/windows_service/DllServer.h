#pragma once

#include <WinSock2.h>
#include <iostream>

class DllServer {
public:
	DllServer(unsigned short int, bool);
	SOCKET WaitForDllClient();
	bool sendall(class Connection&, const char * data, const int totalBytes);
	bool recvall(Connection&, char * data, int totalBytes);
	bool getUInt(Connection& connection, unsigned int &);
	bool getUChar(Connection& connection, unsigned char & );
	~DllServer();
	SOCKADDR_IN m_addr;
	SOCKET m_sListen;
};