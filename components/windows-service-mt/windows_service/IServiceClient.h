#pragma once
/*
 * Interface used for communication with server.
 */


/* <<<interface>>>*/
class IServiceClient
{
	virtual void initialize() = 0;
	virtual void send_data(void *buffer, int buffer_len) = 0;
	virtual int recv_data(void *buffer, int max_len) = 0;
	virtual void finalize() = 0;
	virtual bool connect_to(char* host, char* port) = 0;
	virtual void disconnect() = 0;
};

