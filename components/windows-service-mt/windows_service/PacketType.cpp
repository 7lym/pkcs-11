#include "PacketType.h"
#include <string> 

ShortPacket::ShortPacket()
{
}

ShortPacket::ShortPacket(const unsigned char* message, int size)
{
	this->m_message = new unsigned char[size];
	memcpy(this->m_message, message, size);
	this->size = size;
}

unsigned char * ShortPacket::getContent()
{
	return this->m_message;
}

int ShortPacket::getSize()
{
	return this->size;
}

ShortPacket::~ShortPacket()
{
	delete[] this->m_message;
}

bool Connection::operator==(const Connection& p1)
{
	return this->m_socket == p1.m_socket;
}
