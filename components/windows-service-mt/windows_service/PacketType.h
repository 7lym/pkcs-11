#pragma once


#define PSID_LENGTH 20
#define CK_OK 0
#define CKR_SESSION_HANDLE_INVALID 0x000000b3

enum class PacketType
{
	None = 0,
	Initialize = 1,
	Finalise = 2,
	GetFunctionList = 3,
	GetInfo = 4,
	GetSlotList = 5,
	GetSlotinfo = 6,
	GetMechanismList = 7,
	GetMechanisminfo = 8,
	GetTokenInfo = 9,
	OpenSession = 10,
	CloseSession = 11,
	CloseAllSessions = 12,
};

class ShortPacket
{
public:
	ShortPacket();
	ShortPacket(const unsigned char* message, int size);
	unsigned char* getContent();
	int getSize();
	~ShortPacket();
private:
	unsigned char* m_message = nullptr;
	int size = 0;
};

#include <WinSock2.h>
#include <thread>
class Connection
{
public:
	Connection() {}
	Connection(SOCKET socket) : m_socket(socket) {}

	bool operator==(const Connection& p1);
	std::thread * clientThread;
	int m_ID = 0;
	SOCKET m_socket;
};