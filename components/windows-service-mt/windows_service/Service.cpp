#pragma once
#include "Service.h"
#include <WS2tcpip.h>
#include <iostream>
#include <string>
#include <queue> //for std::queue
#include <mutex> //for std::mutex
#include "ServiceThreadInstance.h"
#pragma comment(lib,"ws2_32.lib") //Required for WinSock
#include "DllServer.h"

void Service::ClientHandlerThread(Service & service, Connection& connection, DllServer * server) //ID = the index in the SOCKET connections array
{
	ServiceThreadInstance applicationInstanceServer(connection.m_socket, server->m_addr, server);
	applicationInstanceServer.CommandListener(connection);
	service.DisconnectClient(connection);
}

void Service::DisconnectClient(Connection& connection) //Disconnects a client and cleans up socket if possible
{
	std::lock_guard<std::shared_mutex> lock(m_mutex_connectionMgr); //Lock connection manager mutex since we are possible removing element(s) from the vector
	closesocket(connection.m_socket); //Close the socket for this connection

	m_connections.erase(std::remove(m_connections.begin(), m_connections.end(), connection)); //Remove connection from vector of connections
	std::cout << "Cleaned up client: " << connection.m_ID << "." << std::endl;
	std::cout << "Total connections: " << m_connections.size() << std::endl;
}


Service::Service(int port, bool loopBacktoLocalHost)
{
	server = new DllServer(port, loopBacktoLocalHost);
	
	m_IDCounter = 0;
}

bool Service::ListenForNewConnection()
{
	SOCKET newConnectionSocket = server->WaitForDllClient();
	if (newConnectionSocket == -1)
	{
		std::cout << "Failed to accept the client's connection." << std::endl;
		return false;
	}

	std::lock_guard<std::shared_mutex> lock(m_mutex_connectionMgr);
	Connection newConnection(newConnectionSocket);
	
	newConnection.m_ID = m_IDCounter;
	m_IDCounter += 1;
	std::thread* CHT = new std::thread(ClientHandlerThread, std::ref(*this), newConnection, server);
	newConnection.clientThread = CHT;

	m_connections.push_back(newConnection);
	std::cout << "Client Connected! ID:" << newConnection.m_ID << std::endl;
	//CHT->detach();
	return true;

}

Service::~Service()
{
	while (m_connections.size() > 0)
		m_connections.front().clientThread->join();
		
	delete server;
}




