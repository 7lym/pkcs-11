#pragma once

#include <list>
#include <shared_mutex>
#include "PacketType.h"



class Service
{
public: 
	
	Service(int port, bool loopBacktoLocalHost = true);
	~Service();
	bool ListenForNewConnection();

private:
	void DisconnectClient(Connection& connection);
	static void ClientHandlerThread(Service & server, Connection& connection, class DllServer *);


	class DllServer * server = nullptr;
	std::list<Connection> m_connections;
	std::shared_mutex m_mutex_connectionMgr; 
	int m_IDCounter = 0;
	//std::vector<std::thread*> m_threads; 
};