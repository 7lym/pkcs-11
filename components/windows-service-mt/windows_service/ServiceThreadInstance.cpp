#pragma once


#include <iostream>
#include <shared_mutex>
#include "SessionManager.h"
#include "PacketType.h"
#include "TLSClient.h"
#include "DllServer.h"
#include "ServiceThreadInstance.h"
#include <WinSock2.h>
bool ServiceThreadInstance::connectedToServer = false;

std::mutex ServiceThreadInstance::masterMutex;
TLSClient ServiceThreadInstance::tokenRemoteServer;
int ServiceThreadInstance::instaceId = 0;

ServiceThreadInstance::ServiceThreadInstance(SOCKET socket, SOCKADDR_IN sockaddr, DllServer * svr)
{
	dllServer = svr;
	if (!connectedToServer)
	{
		//ServiceThreadInstance::connectedToServer = ServiceThreadInstance::ConnectTokenServer();
		;
		this->lastSendPacketType = PacketType::None;

		//TODO: watch it!!! See about that allocation
		this->m_clientPacket = std::make_shared<ShortPacket>();
		this->m_serverPacket = std::make_shared<ShortPacket>();
	}
}

bool ServiceThreadInstance::CommandListener(Connection& oldConnection)
{
	this->sessionManager = SessionManager::gInstance(); // get session instance

	ServiceThreadInstance::instaceId++;


	this->m_clientConnection = oldConnection;


	while (true)
	{

		//get form client app
		if (this->ProcessClientPacket())
			if (this->shortPacket == true)
			{
				std::shared_ptr<ShortPacket> receptionat = nullptr;
				this->masterMutex.lock();
				bool rez = 	ServiceThreadInstance::ConnectTokenServer();
				// send to server -- ssl
				if (rez)
				{
					tokenRemoteServer.send_packet(this->m_clientPacket->getContent(), this->m_clientPacket->getSize());

					//get server response
					receptionat = tokenRemoteServer.receive_packet();

					ServiceThreadInstance::DisconnectTokenServer();
				}
				this->masterMutex.unlock();

				//process server response
				
				if (receptionat != nullptr && rez == true)
					rez = this->receiveServerFullCommand(receptionat);
				/* Send the packet to client */
				/* First send the length */
				if (rez == true)
				{
					int32_t packetSize = m_serverPacket->getSize();
					dllServer->sendall(m_clientConnection, (const char*)&packetSize, sizeof(int32_t));

					/* Then send the actual packet */
					dllServer->sendall(m_clientConnection, (const char*)this->m_serverPacket->getContent(), this->m_serverPacket->getSize());
					if (this->lastSendPacketType == PacketType::Finalise)
						break; //exit thread
				}
				else //erori cand am primit raspunsul de la svr/l am prelucrat
				{
					int32_t lungime = 4;
					dllServer->sendall(m_clientConnection, (const char*)&lungime, sizeof(int32_t));

					unsigned char rasp[] = { 0x05, 0x00, 0x00, 0x00 };
					/* Then send the actual packet */
					dllServer->sendall(m_clientConnection, (const char*)rasp, lungime);
					break;
				}

			}
			else
			{
				
				//packet lung in fisier -- NEIMPLEMENTAT
			}
		else //let the server know client has exit
		{
			int x = 0;
			unsigned char executeorder66[21];
			memcpy(executeorder66, sessionManager->gPSid(this->processID), 20);
			executeorder66[20] = 2;
			int32_t lungime = 21;
			bool rez = ServiceThreadInstance::ConnectTokenServer();
			// send to server -- ssl
			if (rez)
			{
				this->m_clientPacket.reset(new ShortPacket((const unsigned char*)executeorder66, lungime));
				tokenRemoteServer.send_packet(this->m_clientPacket->getContent(), this->m_clientPacket->getSize());

				//get server response
				std::shared_ptr<ShortPacket> receptionat = tokenRemoteServer.receive_packet();

				ServiceThreadInstance::DisconnectTokenServer();
			}
			sessionManager->removePid(processID);
			break; //a esuat ceva la preluare/prelucrare pachet de la dll spre service
		}
	}

	return true;
}



bool ServiceThreadInstance::ProcessClientPacket()
{
	unsigned int totalSize = 0;

	
	// Get the total size of the incomming packet
	if (!dllServer->getUInt(m_clientConnection, totalSize))
		return false;


	// get process ID
	if (!dllServer->getUInt(m_clientConnection, this->processID))
		return false;
	
	// get packet type
	unsigned char fid;
	if (!dllServer->getUChar(m_clientConnection, fid))
		return false;
	this->lastSendPacketType = (PacketType)fid;

	// get the rest
	if (!this->receiveDllFullCommand(m_clientConnection, totalSize - sizeof(int32_t) - 1, this->lastSendPacketType, this->processID))
		return false;
	
	return true;
}


/**
 * Used to receive payload from client application (dll) and 
 * prepare the package to be sent to server
 */
bool ServiceThreadInstance::receiveDllFullCommand(Connection& connection,int totalSize,PacketType packetType,int processID)
{
	if (ServiceThreadInstance::commandMaximumLength >= totalSize) // if short
	{
		this->shortPacket = true;

		unsigned int size_int32 = sizeof(unsigned int);
		int recvCursor = 0;

		if (packetType != PacketType::Initialize)
			recvCursor = PSID_LENGTH + 1; //pt 8 svr

		unsigned char* recvBuffer = new unsigned char[totalSize + recvCursor /*+ 1*/];		// 1 -- PCKS11 function identificator
		
		if (dllServer->recvall(connection, (char*)recvBuffer + recvCursor /*+ 1*/, totalSize))
		{
			int serverPacketSize = totalSize + recvCursor; //la init vrea 8
			/*if (packetType != PacketType::Initialize)
				serverPacketSize++;*/
			//verificare Hid
			if ( packetType == PacketType::CloseSession || packetType > PacketType::CloseAllSessions )
			{
				// daca exista hID in payload
				memcpy(&currentHId, recvBuffer + recvCursor /*+ 1*/, sizeof(unsigned int));
				if (!this->sessionManager->hasHid(processID, currentHId))
				{
					unsigned int badResponse = CK_OK;
					char badBuffer[8];
					memcpy(badBuffer, &size_int32, size_int32);
					badResponse = CKR_SESSION_HANDLE_INVALID;
					memcpy(badBuffer + size_int32, &badResponse, sizeof(unsigned int));

					dllServer->sendall(this->m_clientConnection, (const char*)badBuffer, sizeof(unsigned int));
					dllServer->sendall(this->m_clientConnection, (const char*)badBuffer + size_int32, sizeof(unsigned int));

					return false;
				}
			}

			//daca nu e initialize, NAT-am din pid in psid
			if (packetType != PacketType::Initialize)
			{
				const unsigned char* psid = sessionManager->gPSid(processID);
				if (psid != nullptr)
					memcpy(recvBuffer, psid, PSID_LENGTH);
				else
				{
					unsigned int badResponse = CK_OK;
					char badBuffer[8];
					memcpy(badBuffer, &size_int32, size_int32);
					badResponse = 0x00000005;
					memcpy(badBuffer + size_int32, &badResponse, sizeof(unsigned int));

					dllServer->sendall(this->m_clientConnection, (const char*)badBuffer, sizeof(unsigned int));
					dllServer->sendall(this->m_clientConnection, (const char*)badBuffer + size_int32, sizeof(unsigned int));

					return false; // psid not found -- initialise
				}

				//pune fid 
				memcpy(recvBuffer + recvCursor - 1, &packetType, 1);//-1 pt ca deja am adunat +1 sus
			}


			// HERE WAS THE SWITCH
			this->m_clientPacket.reset(new ShortPacket((const unsigned char*)recvBuffer, serverPacketSize));
			delete[] recvBuffer;
			//HERE WAS A SECURE SEND. Moved logic inside mutex
			return true;
		}
		else
			delete[] recvBuffer;
	}
	else
	{
		this->shortPacket = false;
		//if (!this->receiveDllAllToFile(connection, totalSize,packetType,processID))
			return false;
		//return true;
	}
	return false;
}


bool ServiceThreadInstance::receiveServerFullCommand(std::shared_ptr<ShortPacket> receptionat)
{
	int totalSize = receptionat->getSize();
	unsigned char* recvBuffer = receptionat->getContent();


	if (ServiceThreadInstance::commandMaximumLength >= totalSize)
	{
			this->shortPacket = true;
			unsigned int flag = CK_OK;
			if (memcmp(recvBuffer, &flag, 4) == 0)
			{

				switch (this->lastSendPacketType)
				{
				case PacketType::Initialize:
				{
					/* recvBuffer will be like:

					------------------------------------------
					|  ERR_CODE  |           PSID            |
					|     4      |            20             |
					------------------------------------------

					*/
					this->sessionManager->addPid(this->processID);
					this->sessionManager->sPSid(this->processID, recvBuffer + 4); //received data skipping CK_OK status

					this->m_serverPacket.reset(new ShortPacket(recvBuffer, 4));
				} break;

				case PacketType::OpenSession:
				{
					unsigned int hId = 0;
					/* recvBuffer will be like:

					--------------------------
					|  ERR_CODE  |    HID    |
					|     4      |     4     |
					--------------------------

					*/
					memcpy(&hId, recvBuffer + 4, sizeof(unsigned int));
					this->sessionManager->addHid(this->processID, hId);
				} break;

				case PacketType::Finalise:
				{
					this->sessionManager->removePid(this->processID);
				} break;

				case PacketType::CloseSession:
				{
					this->sessionManager->removeHid(this->processID, this->currentHId);
				} break;

				case PacketType::CloseAllSessions:
				{
					this->sessionManager->removeAllHid(this->processID);
				} break;

				default:
					/* recvBuffer will be like:

					--------------------------------
					|  ERR_CODE  |     Payload     |
					|     4      |    ~variable~   |
					--------------------------------

					*/
					
					break;
				}
				if (lastSendPacketType != PacketType::Initialize)
					this->m_serverPacket.reset(new ShortPacket(recvBuffer, totalSize));
				//aici tica vreau sa vezi daca pot scapa de reset si sa asignez direct receptionat lui serverpacket..fara mmmrleaks
			}
			else
				this->m_serverPacket.reset(new ShortPacket(recvBuffer, totalSize));
			/*else

				return false;*/ //nu avem niciun drept sa nu si ia eroarea direct din svr
	}
	else
	{
		/* NOT TESTED !!! */
		this->shortPacket = false;
		//this->receiveServerAllToFile(m_serverConnection, totalSize);
		return false;
	}
	
	return true;
}








bool ServiceThreadInstance::ConnectTokenServer()
{
	tokenRemoteServer.initialize();
	return connectToRemoteServer("config.txt");
}

bool ServiceThreadInstance::DisconnectTokenServer()
{
	tokenRemoteServer.finalize();
	return true;
}

bool ServiceThreadInstance::connectToRemoteServer(char * configFile)
{
	FILE *fd;
	int N;
	char ip[16];
	char port[6];

	fd = fopen(configFile, "r");

	fscanf(fd, "N=%d\n", &N);
	bool rez = false;
	for (int i = 0; i < N; i++)
	{
		fscanf(fd, "ip=%16[^;];port=%6[^;]", ip, port);
		if (tokenRemoteServer.connect_to(ip, port) == true)
		{
			rez = true;
			break;
		}
	}
	fclose(fd);
	return rez;
}
