#pragma once



class ServiceThreadInstance
{
public:
	
	ServiceThreadInstance(SOCKET socket, SOCKADDR_IN sockaddr, class DllServer *);

	virtual bool CommandListener(Connection& oldConnection);
	
protected:

	virtual bool receiveDllFullCommand(Connection& connection,int totalSize,PacketType packetType,int processID);
	virtual bool receiveServerFullCommand(std::shared_ptr<ShortPacket>);

private:
	bool ProcessClientPacket();

	static bool ConnectTokenServer();
	static bool DisconnectTokenServer();
	static const char* temporaryFile() 
	{
		char tempfilename[10];
		return _itoa(ServiceThreadInstance::instaceId,tempfilename,10);
	}
	static const char* getServerIP() { return "127.0.0.1"; }

private:
	static class TLSClient tokenRemoteServer;
	static std::mutex masterMutex;
	class DllServer * dllServer;
	
public:
	static int instaceId;
	static const int commandMaximumLength = 1024 * 1024 * 5; // 5 MB length allowed
	
	static bool connectedToServer;
private:
	static bool connectToRemoteServer(char *configFile);
	class SessionManager* sessionManager;

	SOCKET m_appSocket;

	Connection m_clientConnection;

	std::shared_ptr<ShortPacket> m_serverPacket;
	std::shared_ptr<ShortPacket> m_clientPacket;

	PacketType lastSendPacketType;


	unsigned int processID;
	bool shortPacket;
	unsigned int currentHId;


};

