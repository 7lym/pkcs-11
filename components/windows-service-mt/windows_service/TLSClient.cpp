
#include <openssl\ssl.h>

#include "TLSClient.h"



TLSClient::TLSClient(){
	ctx =		nullptr;
	serverBio = nullptr;
	ssl =		nullptr;
	flags = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
	initialize();
}

void TLSClient::initialize()
{
	sslMethod = TLS_method();
	ctx = SSL_CTX_new(sslMethod);
	SSL_CTX_set_options(ctx, flags);
	serverBio = BIO_new_ssl_connect(ctx);
	outputBio = BIO_new_fp(stdout, BIO_NOCLOSE);
}

void TLSClient::send_data(void *buffer, int buffer_len)
{
	BIO_write(serverBio, buffer, buffer_len);
}

int TLSClient::recv_data(void *buffer, int max_len)
{
	if (buffer == NULL || max_len == 0) {
		return -1;
	}
	
	int bytesReceived = 0;

	while (bytesReceived < max_len) {
		bytesReceived += BIO_read(serverBio, (char*)buffer + bytesReceived, max_len - bytesReceived);
		if (bytesReceived < 0)
			return -1;
	}
	return bytesReceived;
}

void TLSClient::finalize()
{
	SSL_CTX_free(ctx);
	BIO_free(serverBio);
	BIO_free(outputBio);
}

/*TODO: Error handling*/
bool TLSClient::connect_to(char* host, char* port)
//de bagat exception handling -- server is not up
{
	char socketString[22];
	snprintf(socketString, 22, "%s:%s", host, port);
	BIO_set_conn_hostname(serverBio, socketString);
	BIO_get_ssl(serverBio, &ssl);

	SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);
	SSL_set_tlsext_host_name(ssl, host);

	//outputBio = BIO_new_fp(stdout, BIO_NOCLOSE);
	int rez = BIO_do_connect(serverBio);
	if (rez != 1)
	{
		finalize();
		return false;
	}
	rez = BIO_do_handshake(serverBio);
	if (rez != 1)
	{
		finalize();
		return false;
	}
	return true;
}

void TLSClient::disconnect()
{
}


std::shared_ptr<ShortPacket> TLSClient::receive_packet()
{
	int32_t packet_size = 0;
	unsigned char* packet_buffer = nullptr;
	int x = recv_data(&packet_size, sizeof(int32_t));
	if (x == -1)
		return nullptr;

	char ok = 1;
	send_data(&ok, 1);

	packet_buffer = new unsigned char[packet_size];

	x = recv_data(packet_buffer, packet_size);
	if (x == -1)
	{
		delete[] packet_buffer;
		return nullptr;
	}
	send_data(&ok, 1);

	ShortPacket* packet = new ShortPacket(packet_buffer, packet_size);

	return std::shared_ptr<ShortPacket>(packet);
}

void TLSClient::send_packet(void * buffer, int buffer_len)
{
	/* Send the length of the packet */
	send_data(&buffer_len, sizeof(buffer_len));
	char ok;

	recv_data(&ok, 1);
	/* Send the actual packet */
	send_data(buffer, buffer_len);
	recv_data(&ok, 1);
}

TLSClient::~TLSClient()
{
	finalize();
}
