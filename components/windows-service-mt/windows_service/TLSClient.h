#pragma once

#include <openssl\ssl.h>
#include "IServiceClient.h"
#include "PacketType.h"
#include <string> 
#include <memory> 

class TLSClient :
	public IServiceClient
{
private:
	const char* const PREFERRED_CIPHERS = "HIGH:!aNULL:!kRSA:!SRP:!PSK:!CAMELLIA:!RC4:!MD5:!DSS";

	SSL_CTX		*ctx;			/// framework to establish TLS/SSL connections
	BIO			*serverBio;		/// I/O stream abstraction
	BIO			*outputBio;
	SSL			*ssl;
	const SSL_METHOD	*sslMethod;
	long flags;

public:
	TLSClient();

	void initialize();
	void send_data(void *buffer, int buffer_len);
	int recv_data(void *buffer, int max_len);
	void finalize();
	bool connect_to(char* host, char* port = "443");
	std::shared_ptr<ShortPacket> receive_packet();
	void send_packet(void *buffer, int buffer_len);
	void disconnect();

	~TLSClient();
};

