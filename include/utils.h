#pragma once


#define LOCALHOST "127.0.0.1"
#define LOG() printf("%s:%d", __func__, __LINE__)

#define DUMP(buffer, length) do{for(int i=0;i<length;i++) printf("0x%X ",buffer[i]);}while(0)
